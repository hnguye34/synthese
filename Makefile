#
# Les variables d'environnement libG3X, incG3X
# sont definies dans le fichier ~/.bashrc par le script ../install.sh
#
#compilateur
CC = gcc

CCEXT=
ifeq ($(CC),g++) #en mode debug
  CCEXT=++
endif

#compil en mode 'debug' ou optmis�e (-O2)
DBG = no

ifeq ($(DBG),yes) #en mode debug
  CFLAGS = -g -Wpointer-arith -Wall -ansi
else               #en mode normal
  CFLAGS = -O2 -ansi
endif

# assemblage des infos de lib. et inc.
lib = -lg3x.64b -lm -lGL -lGLU -lglut -L$(libG3X)$(CCEXT)
# fichiers *.c locaux
src = src/
# fichiers *.h locaux et lib.
inc = -I./include -I$(incG3X) -I/usr/include/GL

exec = objets scene

all : $(exec) clean

OBJ = Object.o quadrilateral.o sphere.o parabol.o cube.o cylinder.o cone.o tore.o spring.o rectangle.o flight.o  part1_main.o  tetrahedron.o dome.o motor.o scene.o

SRCS = $(OBJ:$(src)%.o=$(src)%.c)

# r�gle g�n�rique de cr�ation de xxx.o � partir de src/xxx.c
%.o : $(src)%.c
	@echo "module $@"
	@$(CC) $(CFLAGS) $(inc) -c $< -o $@
	@echo "------------------------"

# r�gle g�n�rique de cr�ation de l'executable xxx � partir de src/xxx.c (1 seul module)


objets : Object.o quadrilateral.o sphere.o parabol.o rectangle.o cube.o cylinder.o cone.o tore.o spring.o tetrahedron.o  part1_main.o 
	@echo "assemblage [$^]->$@"
	@$(CC) $^ $(lib) -o $@
	@echo "------------------------"

	

scene : Object.o quadrilateral.o cylinder.o cone.o sphere.o  flight.o dome.o motor.o scene.o 
	@echo "assemblage [$^]->$@"
	@$(CC) $^ $(lib) -o $@
	@echo "------------------------"
	
.PHONY : clean cleanall ?

# informations sur les param�tres de compilation
? :
	@echo "---------compilation informations----------"
	@echo "  processor      : $(PROCBIT)"
	@echo "  compiler       : $(CC)"
	@echo "  options        : $(CFLAGS)"
	@echo "  lib g3x/OpenGl : $(lib)"
	@echo "  headers        : $(incG3X)"
clean :
	@rm -f $(OBJ) 
cleanall :
	@rm -f $(OBJ) $(exec)

