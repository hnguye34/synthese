#ifndef   ___CUBE___
#define  ___CUBE___

#include <stdio.h>

#define SLICE_NUM 		30
#define DIRECTION_NUM  	6
#define SIDE_NUM  		12
#define POINT_NUM 		SLICE_NUM+1


typedef struct {
	G3Xpoint	points[2][POINT_NUM];	
	G3Xvector 	normal ;
} CubeFace;
	
typedef struct {
    G3Xpoint  vertex[POINT_NUM ];
    G3Xvector normal[DIRECTION_NUM];
    int positionNormal[DIRECTION_NUM];
    int curPosition;
    G3Xvector xNormal ;
    G3Xvector yNormal ;
    G3Xvector zNormal ;
    double initialCamDist ;
    CubeFace		faces[4] ;
}Cube;


void build_cube(Cube* object) ;
void drawCube(Cube * object) ;

#endif
