#ifndef   ___RECTANGLE___
#define  ___RECTANGLE___

#include <stdio.h>

#define RECT_LENG_MAX_POINTS_NUM	20
#define RECT_WITDH_MAX_POINTS_NUM	20

	
typedef struct {
	int length;
	int width;
    G3Xpoint  firstLongSide[RECT_LENG_MAX_POINTS_NUM ];
    G3Xpoint  secondLongSide[RECT_LENG_MAX_POINTS_NUM ];
    G3Xpoint  firstShortSide[RECT_LENG_MAX_POINTS_NUM ];
    G3Xpoint  secondShortSide[RECT_LENG_MAX_POINTS_NUM ];
    G3Xvector normal;
  
}Rectangle;

void build_rectangle(Rectangle* rectangle, int lengh, int width) ;
void drawRectangle(Rectangle * rectangle) ;

#endif
