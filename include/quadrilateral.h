#ifndef   ___QUADRILATERAL___
#define  ___QUADRILATERAL___

#include <stdio.h>

#define QUAD_MAX_SLICES 	5
#define QUAD_MAX_POINTS 	QUAD_MAX_SLICES + 1

typedef struct {
	G3Xpoint ab[QUAD_MAX_POINTS] ;
	G3Xpoint ac[QUAD_MAX_POINTS] ;
	G3Xpoint D ;
	G3Xvector 	normals[2] ;
	double AB;
	double BAC; /*angles*/
	double AC;
	double CD;
	double ACD; /*angles */
	double thickness;
} QuadroSides;

	
typedef struct {
	QuadroSides face1 ;
	QuadroSides face2 ;
	G3Xvector 	normals[4] ;
	double thickness;
}Quadrilateral;


void build_quadrilateral(Quadrilateral* quadrilateral, double AC, double AB, double CD , double BAC, double ACD,  double thickness) ;
void drawQuadrilateral(Quadrilateral * quadrilateral) ;

#endif
