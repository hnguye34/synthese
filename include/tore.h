#ifndef ___TORE__
#define ___TORE__

#include <stdio.h>
#include <g3x.h>

#define TORE_POINTS_NUM_BY_CIRCLE 		30 
#define TORE_CIRCLE_NUM 				26
#define TORE_POINT_NUM 					TORE_CIRCLE_NUM * 2 *  TORE_POINTS_NUM_BY_CIRCLE

   
typedef struct {
    G3Xpoint  points[ TORE_POINT_NUM ];
	G3Xvector normals[TORE_POINT_NUM];
}Tore;


void build_tore( Tore * tore);
void drawTore( Tore * tore);

#endif
