#ifndef ___MOTOR__
#define ___MOTOR__

#include <stdio.h>
#include <g3x.h>
#include <cone.h>
#include <cylinder.h>
#include <quadrilateral.h>
#include <sphere.h>

#define MOTOR_BLADES_NUM	4   
   
typedef struct {
	Quadrilateral 	blades[MOTOR_BLADES_NUM];
	Cylinder  		engine;
	G3Xcolor 		color ;
	G3Xpoint		center;
} Motor ;

void build_motor( Motor * flight ,  G3Xcolor color);
void rotate_motor(Motor* motor, double angle, AXIS axis);
void start_motor ( Motor* motor, double speed, AXIS axis );

#endif
