#ifndef   ___DOME___
#define  ___DOME___

#include <stdio.h>
#include "../include/quadrilateral.h"


typedef struct {
	G3Xpoint ab[QUAD_MAX_POINTS] ;
	G3Xpoint ac[QUAD_MAX_POINTS] ;
	G3Xpoint D ;
	G3Xvector 	normals[2] ;
	double AB;
	double BAC; /*angles*/
	double AC;
	double CD;
	double ACD; /*angles */
	double thickness;
} DomeSides;

	
typedef struct {
	DomeSides face1 ;
	DomeSides face2 ;
	G3Xvector 	normals[4] ;
	double thickness;
}Dome;



#endif
