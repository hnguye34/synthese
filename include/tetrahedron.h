#pragma once
#ifndef ___Tetrahedron___
#define ___Tetrahedron___

#include <stdio.h>
#include <g3x.h>

#define TETRA_SIDE_POINTS_MAXNUM                 20
#define TETRA_SIDE_NUM                             4
#define TETRA_DISTANCE_TOP_TO_CENTER             1

typedef struct {
    G3Xpoint  top;
    G3Xpoint  base1;
    G3Xpoint  base2;
    G3Xpoint  sidePoint1[TETRA_SIDE_POINTS_MAXNUM];
    G3Xpoint  sidePoint2[TETRA_SIDE_POINTS_MAXNUM];
    G3Xvector normal ;

}Face;

typedef struct {
    G3Xpoint        top;
    double          angle;
    Face            faces[TETRA_SIDE_NUM];
} Tetrahedron;




void build_tetrahedron(Tetrahedron * tetrahedron, double angle);
void drawTetrahedron(Tetrahedron * tetrahedron);

#endif

