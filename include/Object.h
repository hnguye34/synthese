#ifndef __DATA___
#define __DATA___

#include <g3x.h>

typedef enum  { AXIS_X = 1, AXIS_Y = 2, AXIS_Z = 3 }  AXIS; 

typedef struct {
    int pointNum;
    G3Xpoint * vertex;
    G3Xvector * vector;
}Object;

void drawPoint (G3Xpoint face[], int position);

void drawAPoint (const G3Xpoint * point);
void copyPoint( G3Xpoint * a, G3Xpoint *b );
void dumpPoint( G3Xpoint * a, const char * text );
void rotate(G3Xpoint points[], int pointsNum,  G3Xvector normals[], int normalsNum,  double angle, AXIS axis) ;
void scale(G3Xpoint points[], int pointsNum, G3Xvector normals[], int normalsNum, double x, double y, double z);

void translate(G3Xpoint points[], int pointsNum,  double x, double y, double z);
#endif
