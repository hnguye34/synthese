#ifndef ___FLIGHT__
#define ___FLIGHT__

#include <stdio.h>
#include <g3x.h>
#include <cone.h>
#include <cylinder.h>
#include <quadrilateral.h>
#include <sphere.h>
#include <motor.h>  
   
typedef struct {
    Cylinder  		cockpit;
	Cone 			head;
	Quadrilateral 	rWing; 
	Quadrilateral 	lWing;
	Quadrilateral 	steering;
	Quadrilateral 	r_tail;
	Quadrilateral 	l_tail;
	Sphere 			exit ;
	G3Xcolor		exitColor;
	G3Xcolor		cockpitColor;
	G3Xcolor		wingColor;
	Motor 			r_motor;
	Motor 			l_motor;

}Flight;

void translate_flight(Flight * flight, double x, double y, double z ) ;
void rotate_flight(Flight * flight, double angle, AXIS axis);
void scale_flight(Flight * flight, double x, double y, double z );
void build_flight( Flight * flight ,  G3Xcolor		exitColor,	G3Xcolor cockpitColor,	G3Xcolor		wingColor);
void start_flight( Flight * flight, double speed );
void scale_sphere(Sphere * sphere, double x, double y, double z);
#endif
