#ifndef   ___PARBOL___
#define  ___PARBOL___

#include <stdio.h>

#define PAR_POINTS_NUM 		30

typedef struct {
	G3Xpoint  points[ PAR_POINTS_NUM ];
}Parabol;


void build_parabol(Parabol* parabol) ;
void drawParabol(Parabol * object) ;

#endif
