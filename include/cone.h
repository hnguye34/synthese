#ifndef ___CONE__
#define ___CONE__

#include <stdio.h>
#include <g3x.h>


#define CONE_CIRCLE_POINTS_NUM		200 
#define CONE_LEVEL_NUM				20 


typedef struct {
	G3Xpoint  	points[ CONE_CIRCLE_POINTS_NUM ];
    G3Xpoint 	center;
    int			indice;
} Circle;


typedef struct {
	Circle 		circles[CONE_LEVEL_NUM + 1];
	G3Xvector 	normals[CONE_LEVEL_NUM ][CONE_CIRCLE_POINTS_NUM] ;
	G3Xpoint 	top;
}Cone;


void build_cone( Cone * cone);
void drawCone( Cone * cone);

#endif
