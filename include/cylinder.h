/* ===============================================================
  E.Incerti - Universit� de Marne-la-Vall�e - incerti@univ-mlv.fr
  ============================================================= */
#ifndef ___CYLINDER__
#define ___CYLINDER__

#include <stdio.h>
#include <g3x.h>
#include <Object.h>


#define CYL_SIDE_SLICE_NUM 		40
#define CYL_SIDE_POINT_NUM 		CYL_SIDE_SLICE_NUM * 2 
 

typedef struct {
    G3Xpoint  	face1Points[ CYL_SIDE_POINT_NUM ];
    G3Xpoint  	face2Points[ CYL_SIDE_POINT_NUM ];
    G3Xpoint 	face1Center;
    G3Xpoint 	face2Center;
    G3Xpoint 	center;
    G3Xvector 	sideNormals[ CYL_SIDE_POINT_NUM ];
    G3Xvector 	face1Normals[CYL_SIDE_POINT_NUM];
    G3Xvector 	face2Normals[CYL_SIDE_POINT_NUM];
   
 
}Cylinder;

void rotate_cylinder(Cylinder * cylinder, double angle, AXIS axis );

void gl_Cylinder( double ray, double thinnes);
void build_cylinder( Cylinder * cylinder);
void drawCylinder( Cylinder * cylinder);


#endif
