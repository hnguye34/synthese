#ifndef ___SPRING___
#define ___SPRING___

#include <stdio.h>
#include <g3x.h>

#define SPRING_WIRES_NUM 				8 
#define SPRING_CIRCLE_NUM 				250
#define SPRING_TOUR_NUM					4
#define SPRING_LENGTH					30
#define SPRING_CIRLCE_RAY				0.1
#define SPRING_ELASTIC_DEGREE			0.002


typedef struct {
	G3Xpoint  	points[ 2 *  SPRING_CIRCLE_NUM ];
} Wire;


typedef struct {
	Wire  		wires[ SPRING_WIRES_NUM * 2];
} Spring;



void  build_spring(Spring* spring);

void  drawSpring(Spring* spring);

/*void  build_spring(int numc, int numt, int h, int tour) ; */

#endif
