#ifndef ___SPHERE__
#define ___SPHERE__

#include <stdio.h>
#include <g3x.h>

#define SPH_NBPMAX	100
#define SPH_POINTS_NUM 	SPH_NBPMAX*SPH_NBPMAX

   
typedef struct {
    G3Xpoint  points [SPH_POINTS_NUM];
    G3Xpoint  center;	
	G3Xvector normals [SPH_POINTS_NUM];
	G3Xcolor	colors[2];
}Sphere;

 /*
  * Theta : 2*PI si ellipse complète, PI : moitie Elipse etc...
  * proportion : proportion entre la grand rayon et le petit rayon
  */
void build_sphere( Sphere * sphere, double angleTheta, double proportion);
void drawSphere( Sphere * sphere);
void setColor(Sphere * sphere, G3Xcolor* color, int index);

#endif
