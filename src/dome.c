#include "../include/Object.h"
#include "../include/dome.h"



 void build_domeSides(DomeSides* side) {
	
	/* Settings */
	const double xA = 0;
	const double yA = 0;
	const double zA = 0;
	side->AC = 1;
	side->AB = 3;
	side->CD = 2.5;
	side->BAC = 2*PI/3;
	side->ACD = PI / 3;
	
	int 			i 		= 0;
	double 			stepAB 	= side->AB / QUAD_MAX_SLICES;
	double 	x 		= xA;
	for ( i = 0 ; i < QUAD_MAX_POINTS; i++, x+=stepAB ){
		G3Xset (side->ab[i], x, yA, 0 ); 
	} 
	
	double 	stepAC 	= side-> AC / QUAD_MAX_SLICES;
	double 	y	=  yA;
	x  	=  xA;
	
	for ( i = 0 ; i < QUAD_MAX_POINTS; i++ ){
		
		y 		= i * stepAC * sin(side->BAC) + yA ;
		x 		= i * stepAC * cos(side->BAC) + xA ;
		G3Xset (side->ac[i], x, y, 0 );	
		printf ("--->Angle:%f. Origine:%f\n" , i * stepAC , side->AC);
	} 
	
	
	
	const double sigma = side->ACD - (PI - side->BAC );
	const double xD = xA + side->ac[QUAD_MAX_POINTS-1][0] + side->CD * cos(sigma);
	const double yD = yA + side->ac[QUAD_MAX_POINTS-1][1] + side->CD * sin(sigma) ;
	
 	G3Xset(side->D , xD , yD , 0 );
}

void build_dome(Dome* quadrilateral) {
	build_domeSides(&quadrilateral->face1);
	
	memcpy(&quadrilateral->face2, &quadrilateral->face1, sizeof(DomeSides));
	memcpy(quadrilateral->face2.ab, quadrilateral->face1.ab,QUAD_MAX_POINTS* sizeof(G3Xpoint));
	memcpy(quadrilateral->face2.ac, quadrilateral->face1.ac,QUAD_MAX_POINTS* sizeof(G3Xpoint));
	
	int i = 0;
	for ( i = 0 ; i < QUAD_MAX_POINTS; i++ ){
		quadrilateral->face2.ab[i][2] = -1;
		quadrilateral->face2.ac[i][2] = -1;
	} 
	
}

void drawFaceDome(DomeSides * face) {
	
	
	
	int i = 0;
	glBegin(GL_TRIANGLES);
	
	for ( i = 0 ; i < QUAD_MAX_POINTS-1; i++ ){
		drawAPoint(&face->D);
		drawAPoint(&face->ab[i]);
		drawAPoint(&face->ab[i+1]);
	}
	
	drawAPoint(&face->D);
	drawAPoint(&face->ab[QUAD_MAX_POINTS-2]);
	drawAPoint(&face->ab[QUAD_MAX_POINTS-1]);
	 
	 
	 for ( i = 0 ; i < QUAD_MAX_POINTS-1; i++ ){
		drawAPoint(&face->D);
		drawAPoint(&face->ac[i]);
		drawAPoint(&face->ac[i+1]);
		
	
	}
	
	drawAPoint(&face->D);
	drawAPoint(&face->ac[QUAD_MAX_POINTS-2]);
	drawAPoint(&face->ac[QUAD_MAX_POINTS-1]);
	 
	 
	glEnd();  
	   	
   
}


void drawDome(Dome * quadrilateral) {
	/*drawFaceQuadrilateral (&quadrilateral->face1);*/
	drawFaceDome (&quadrilateral->face2);
}

