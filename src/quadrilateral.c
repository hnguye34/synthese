#include "../include/Object.h"
#include "../include/quadrilateral.h"



void translate_quadriface(QuadroSides* side, double x, double y, double z ) {
	translate(side->ab, QUAD_MAX_POINTS, x, y, z);
	translate(side->ac, QUAD_MAX_POINTS, x, y, z);
	translate(&side->D, 1, x, y, z);
}

void translate_quadrilateral(Quadrilateral * quadrilateral, double x, double y, double z ) {
	translate_quadriface(&quadrilateral->face1, x, y, z);
	translate_quadriface(&quadrilateral->face2, x, y, z);
}


void rotate_quadrifacel(QuadroSides * side, double angle, AXIS axis ) {
	rotate(side->ab, QUAD_MAX_POINTS, side->normals, 2, angle, axis);
	rotate(side->ac, QUAD_MAX_POINTS, side->normals, 2, angle, axis);
	rotate(&side->D, 1, NULL, 0, angle , axis);
}

void rotate_quadrilateral(Quadrilateral * quadrilateral, double angle, AXIS axis) {
	rotate_quadrifacel(&quadrilateral->face1, angle, axis);
	rotate_quadrifacel(&quadrilateral->face2, angle, axis );
}

void scale_quadriface(QuadroSides* side, double x, double y, double z ) {
	scale(side->ab, QUAD_MAX_POINTS, side->normals, 2, x, y, z);
	scale(side->ac, QUAD_MAX_POINTS, side->normals, 2, x, y, z);
	scale(&side->D, 1, NULL, 0, x, y, z);
}

void scale_quadrilateral(Quadrilateral * quadrilateral, double x, double y, double z  ) {
	scale_quadriface(&quadrilateral->face1, x, y, z);
	scale_quadriface(&quadrilateral->face2, x, y, z);
}



 void build_quadroSides(QuadroSides* side, double z) {
	
	/* Settings */
	const double xA = 0;
	const double yA = 0;
	
	
	
	int 			i 		= 0;
	double 			stepAB 	= side->AB / QUAD_MAX_SLICES;
	double 	x 		= xA;
	for ( i = 0 ; i < QUAD_MAX_POINTS; i++, x+=stepAB ){
		G3Xset (side->ab[i], x, yA, z ); 
	} 
	
	double 	stepAC 	= side-> AC / QUAD_MAX_SLICES;
	double 	y	=  yA;
	x  	=  xA;
	
	for ( i = 0 ; i < QUAD_MAX_POINTS; i++ ){
		
		y 		= i * stepAC * sin(side->BAC) + yA ;
		x 		= i * stepAC * cos(side->BAC) + xA ;
		G3Xset (side->ac[i], x, y, z );	
	} 
	
	
	
	const double sigma = side->ACD - (PI - side->BAC );
	const double xD = xA + side->ac[QUAD_MAX_POINTS-1][0] + side->CD * cos(sigma);
	const double yD = yA + side->ac[QUAD_MAX_POINTS-1][1] + side->CD * sin(sigma) ;
	
 	G3Xset(side->D , xD , yD ,  z + side->thickness);
 	
 	normalFromABC (side->normals[0], side->D, side->ab[0], side->ab[QUAD_MAX_POINTS-1] , 1);
 	normalFromABC (side->normals[1], side->D, side->ac[0], side->ac[QUAD_MAX_POINTS-1] , 1);
}

void setquadriThickness1(Quadrilateral* quadrilateral, double thickness) {
	
	quadrilateral->face1.D[2]= quadrilateral->face1.ab[0][2] + thickness;
}

void setquadriThickness2(Quadrilateral* quadrilateral, double thickness) {
	
	quadrilateral->face2.D[2]= quadrilateral->face2.ab[0][2] -thickness;
}


void build_quadrilateral(Quadrilateral* quadrilateral, double AC, double AB, double CD , double BAC, double ACD, double thickness) {
	
	quadrilateral->face1.thickness = 0.0;
	quadrilateral->face1.AC = AC;
	quadrilateral->face1.AB = AB;
	quadrilateral->face1.CD = CD;
	quadrilateral->face1.BAC = BAC;
	quadrilateral->face1.ACD = ACD;
	const double z = thickness/2 ;
	
	build_quadroSides(&quadrilateral->face1, z);
	
	memcpy(&quadrilateral->face2, &quadrilateral->face1, sizeof(QuadroSides));
	memcpy(quadrilateral->face2.ab, quadrilateral->face1.ab,QUAD_MAX_POINTS* sizeof(G3Xpoint));
	memcpy(quadrilateral->face2.ac, quadrilateral->face1.ac,QUAD_MAX_POINTS* sizeof(G3Xpoint));
	
	int i = 0;
	const double face2Z = -thickness/2;
	
	for ( i = 0 ; i < QUAD_MAX_POINTS; i++ ){
		quadrilateral->face2.ab[i][2] = face2Z;
		quadrilateral->face2.ac[i][2] = face2Z;
	} 
	
	quadrilateral->face2.D[2] = face2Z- quadrilateral->face2.thickness ;
	
	normalFromABC (quadrilateral->normals[0], quadrilateral->face1.ab[0], quadrilateral->face1.ab[QUAD_MAX_POINTS-1] , 
							quadrilateral->face2.ab[QUAD_MAX_POINTS-1], 1);
	normalFromABC (quadrilateral->normals[1], quadrilateral->face1.ac[0], quadrilateral->face1.ac[QUAD_MAX_POINTS-1] , 
							quadrilateral->face2.ac[QUAD_MAX_POINTS-1], 1);
	normalFromABC (quadrilateral->normals[3], quadrilateral->face1.D, quadrilateral->face1.ab[QUAD_MAX_POINTS-1] , 
						quadrilateral->face2.ab[QUAD_MAX_POINTS-1], 1);
	
}

void drawFaceQuadrilateral(QuadroSides * face) {
	
	
	
	int i = 0;
	glBegin(GL_TRIANGLES);
	glNormal3dv( face->normals[0]);
	for ( i = 0 ; i < QUAD_MAX_POINTS-1; i++ ){
		drawAPoint(&face->D);
		drawAPoint(&face->ab[i]);
		drawAPoint(&face->ab[i+1]);
	}
	
	drawAPoint(&face->D);
	drawAPoint(&face->ab[QUAD_MAX_POINTS-2]);
	drawAPoint(&face->ab[QUAD_MAX_POINTS-1]);
	 
	glEnd(); 
	
	glBegin(GL_TRIANGLES);
	 glNormal3dv( face->normals[1]);
	 for ( i = 0 ; i < QUAD_MAX_POINTS-1; i++ ){
		drawAPoint(&face->D);
		drawAPoint(&face->ac[i]);
		drawAPoint(&face->ac[i+1]);
		
	
	}
	
	drawAPoint(&face->D);
	drawAPoint(&face->ac[QUAD_MAX_POINTS-2]);
	drawAPoint(&face->ac[QUAD_MAX_POINTS-1]);
	 
	 
	glEnd();  
	   	
   
}


void drawLateral(G3Xpoint side1[], G3Xpoint side2[], G3Xvector normal) {
	
	glBegin(GL_QUADS);
	glNormal3dv( normal);
	int i = 0;
	for ( i = 0 ; i < QUAD_MAX_POINTS-1; i++ ){
		drawAPoint(&side1[i]);
		drawAPoint(&side1[i+1]);
		drawAPoint(&side2[i+1]);
		drawAPoint(&side2[i]);
	}
	
	drawAPoint(&side1[QUAD_MAX_POINTS-2]);
	drawAPoint(&side1[QUAD_MAX_POINTS-1]);
	drawAPoint(&side2[QUAD_MAX_POINTS-1]);
	drawAPoint(&side2[QUAD_MAX_POINTS-2]);
		
	
	glEnd();  
}

void drawQuadrilateral(Quadrilateral * quadrilateral) {
	drawFaceQuadrilateral (&quadrilateral->face1);
	drawFaceQuadrilateral (&quadrilateral->face2);
	
	drawLateral(quadrilateral->face1.ac, quadrilateral->face2.ac , quadrilateral->normals[1]);
	drawLateral(quadrilateral->face1.ab, quadrilateral->face2.ab , quadrilateral->normals[0]);
	
	glBegin(GL_QUADS);
	drawAPoint(&quadrilateral->face1.D);
	drawAPoint(&quadrilateral->face1.ab[QUAD_MAX_POINTS-1]);
	drawAPoint(&quadrilateral->face2.ab[QUAD_MAX_POINTS-1]);
	drawAPoint(&quadrilateral->face2.D);
	
	glEnd();
}

