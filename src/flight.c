#include <stdio.h>
#include <g3x.h>
#include <Object.h>
#include <flight.h>


void translate_flight(Flight * flight, double x, double y, double z ) {
	
	/*printf ("-->translate_flight: (%f ,%f, %f)\n", x, y, z);*/
	translate_motor(&flight->l_motor, x, y, z);
	translate_motor(&flight->r_motor, x, y, z);
	
	translate_quadrilateral(&flight->lWing, x, y, z);
	translate_quadrilateral(&flight->rWing, x, y, z);
	translate_quadrilateral(&flight->r_tail, x, y, z);
	translate_quadrilateral(&flight->l_tail, x, y, z);
	translate_quadrilateral(&flight->steering, x, y, z);
	translate_sphere(&flight->exit, x, y, z);
	translate_cylinder(&flight->cockpit, x, y, z);
}

void rotate_flight(Flight * flight, double angle, AXIS axis) {
	/*printf ("-->rotate_flight anglle: (%f)\n", angle);*/
	rotate_motor(&flight->l_motor, angle, axis);
	rotate_motor(&flight->r_motor, angle, axis);
	rotate_quadrilateral(&flight->lWing, angle, axis);
	rotate_quadrilateral(&flight->rWing, angle, axis);
	rotate_quadrilateral(&flight->r_tail, angle, axis);
	rotate_quadrilateral(&flight->l_tail, angle, axis);
	rotate_quadrilateral(&flight->steering, angle, axis);
	rotate_sphere(&flight->exit, angle, axis);
	rotate_cylinder(&flight->cockpit, angle, axis);
}

void scale_flight(Flight * flight, double x, double y, double z ) {
	/*printf ("-->scale_flight\n");*/
	scale_motor(&flight->l_motor, x, y, z);
	scale_motor(&flight->r_motor, x, y, z);
	scale_quadrilateral(&flight->lWing, x, y, z);
	scale_quadrilateral(&flight->rWing, x, y, z);
	scale_quadrilateral(&flight->r_tail, x, y, z);
	scale_quadrilateral(&flight->l_tail, x, y, z);
	scale_quadrilateral(&flight->steering, x, y, z);
	scale_sphere(&flight->exit, x, y, z);
	scale_cylinder(&flight->cockpit, x, y, z);
	
}

void start_flight ( Flight * flight, double speed )
{
	start_motor(&flight->l_motor, speed, AXIS_Y);
	start_motor(&flight->r_motor, speed, AXIS_Y);
}

void drawFlight( Flight * flight)
{
	
	glPushMatrix();
	g3x_Material(flight->cockpitColor,0.25,0.5,0.,0.,1.);
		
	drawCylinder (&(flight->cockpit)); 
	
	drawQuadrilateral (&flight->steering); 
	drawQuadrilateral (&flight->r_tail); 
	drawQuadrilateral (&flight->l_tail); 
	
	g3x_Material(flight->wingColor,0.25,0.5,0.,0.,1.);
	drawQuadrilateral (&(flight->lWing));
	drawQuadrilateral (&flight->rWing); 
	
	g3x_Material(flight->exitColor,0.25,0.5,0.,0.,1.);
	drawSphere (&flight->exit); 
	
	drawMotor(&flight->l_motor);
	drawMotor(&flight->r_motor);
	
	glPopMatrix(); 
	
}

void build_steerring (Flight * flight, double thickness )
{
	double width = 0.5;
	double frontSide = 0.2;
	double behindSide = 1.0;
	build_quadrilateral (&flight->steering, width, frontSide, behindSide,  PI/2, PI/2, thickness); 
	rotate_quadrilateral (&flight->steering, -PI/2, AXIS_Y);
	translate_quadrilateral(&flight->steering, 0.0, 1.3, 0.3 ); /* x +/- : loin/ pres . y negative: monter vers la tete */ 
}

void build_2tails (Flight * flight, double thickness )
{
	double width = 0.5;
	double frontSide = 0.2;
	double behindSide = 1.0;
	build_quadrilateral (&flight->l_tail, width, frontSide, behindSide,  PI/2, PI/2, thickness); 
	translate_quadrilateral(&flight->l_tail, 0.3, 1.3, 0.0 ); /* x +/- : loin/ pres . y negative: monter vers la tete */ 
	
	build_quadrilateral (&flight->r_tail, width, frontSide, behindSide,  PI/2, PI/2, thickness); 
	rotate_quadrilateral (&flight->r_tail, PI, AXIS_Y);
	translate_quadrilateral(&flight->r_tail, -0.3, 1.3, 0.0 ); /* x +/- : loin/ pres . y negative: monter vers la tete */ 
}



void build_exit (Flight * flight )
{
	build_sphere (&flight->exit, PI, 0.3); 
	scale_sphere(&flight->exit, 0.5, 1.0, 0.8);
	rotate_sphere(&flight->exit, PI/2, AXIS_Y);
	rotate_sphere(&flight->exit, PI/2, AXIS_X);
	translate_sphere(&flight->exit, 0.0, -1.2, 0.3);
	
}

 void build_flight(Flight * flight,  G3Xcolor		exitColor,	G3Xcolor cockpitColor,	G3Xcolor		wingColor)
{
	copyColor(flight->exitColor,  exitColor) ;
	copyColor(flight->wingColor,  wingColor) ;
	copyColor(flight->cockpitColor,  cockpitColor) ;

	build_cylinder (&flight->cockpit); 
	rotate_cylinder (&flight->cockpit, PI/2, AXIS_X); 
	scale_cylinder ( &flight->cockpit, 0.3, 2.0, 0.3);
	setHead1Height ( &flight->cockpit, 1.3);
	
	double thickness = 0.1;
	double cockpitWingSide = 0.6;
	double frontWingSide = 1.7;
	double behindWingSide = 1.2 * frontWingSide ;
	
	
	build_quadrilateral (&flight->lWing, cockpitWingSide, frontWingSide, behindWingSide,  PI/2, PI/2, thickness); 
	build_quadrilateral (&flight->rWing, cockpitWingSide, frontWingSide, behindWingSide,  PI/2, PI/2, thickness ); 
	
	double longCockpitPosition = -0.5; /*negatif: vers le front */

	rotate_quadrilateral (&flight->rWing, PI, AXIS_Y);
	
	translate_quadrilateral(&flight->lWing, 0.3, -0.9, 0.0 ); /* x +/- : loin/ pres . y negative: monter vers la tete */ 
	translate_quadrilateral(&flight->rWing, -0.3, -0.9, 0.0 ); 
	
	build_steerring(flight, thickness);
	
	build_2tails(flight, thickness);
	build_exit(flight);
	build_motor(&flight->l_motor, wingColor);
	build_motor(&flight->r_motor, wingColor);

	rotate_motor(&flight->l_motor, PI, AXIS_X);
	scale_motor(&flight->l_motor, -0.2, -0.2, -0.2);
	rotate_motor(&flight->r_motor, PI, AXIS_X);
	scale_motor(&flight->r_motor, -0.2, -0.2, -0.2);
	
	
	translate_motor(&flight->l_motor, 1.0, -0.8, -0.1); /* z:hauter */
	translate_motor(&flight->r_motor, -1.0, -0.8, -0.1); /* z:hauter */
	
}	


