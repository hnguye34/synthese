#include <stdio.h>
#include <g3x.h>
#include <sphere.h>
#include <Object.h>

void drawSphere_styleeffect( Sphere * sphere)
{
	int i = 0;

	glBegin(GL_QUADS);

	for (i = 0; i < SPH_POINTS_NUM  ; i++)
	{
		glVertex3dv(sphere->points[i]);
		glNormal3dv( sphere->normals[i]);
	}

	glEnd();

 }
 
 
void translate_sphere(Sphere * sphere, double x, double y, double z ) {

	translate(sphere->points, SPH_POINTS_NUM,  x, y, z);
	translate(&sphere->center, 1,  x, y, z);
}



void rotate_sphere(Sphere * sphere, double angle, AXIS axis) {
	
	rotate(sphere->points, SPH_POINTS_NUM, sphere->normals, SPH_POINTS_NUM, angle, axis);
	rotate(&sphere->center, 1, NULL, 0, angle, axis);
	
}

void scale_sphere(Sphere * sphere, double x, double y, double z) {
	scale(sphere->points, SPH_POINTS_NUM, sphere->normals, SPH_POINTS_NUM,  x, y, z);
	scale(&sphere->center, 1, NULL, 0,  x, y, z);
}


void setColor(Sphere * sphere , G3Xcolor* color, int index)
{
	if ( index >1)
	return;
	
	copyColor (&sphere->colors[index], *color);
}

 void drawSphere( Sphere * sphere)
{
	const int n = 30 ; /* nb longitude */ 
	const int p = 30 ; /* nb lattitude */
	int nstep=SPH_NBPMAX/n;
	int pstep=SPH_NBPMAX/p;
	int i,j,k;
	
	int normalNum = 0;
	int iCol = 0;
	
	for (i=0;i<SPH_NBPMAX-pstep;i+=pstep, iCol++) /* lattitude par lattuidue*/ 
	{
		if ( iCol == 2 )
			iCol = 0;
		
		
		for (j=0;j<SPH_NBPMAX-nstep;j+=nstep)
		{
			glBegin(GL_QUADS);
			g3x_Material(sphere->colors[iCol],0.25,0.5,0.,0.,1.);
			k=(i      )*SPH_NBPMAX+j;
			glVertex3dv(sphere->points[k]);
			k=(i      )*SPH_NBPMAX+j+nstep;
			glVertex3dv(sphere->points[k]);
			k=(i+pstep)*SPH_NBPMAX+j+nstep;
			glVertex3dv(sphere->points[k]);
			k=(i+pstep)*SPH_NBPMAX+j;
			glVertex3dv(sphere->points[k]);
			glNormal3dv( sphere->normals[k]);
			glEnd();
		}
		glBegin(GL_QUADS);
		
		glNormal3dv( sphere->normals[k]);
		k=(i      )*SPH_NBPMAX+j;
		glVertex3dv(sphere->points[k]);
		k=(i      )*SPH_NBPMAX;
		glVertex3dv(sphere->points[k]);
		k=(i+pstep)*SPH_NBPMAX;
	
		glVertex3dv(sphere->points[k]);
		k=(i+pstep)*SPH_NBPMAX+j;
		glVertex3dv(sphere->points[k]);
		
		glEnd();
	}
	
 }

 /*
  * Theta : 2*PI si ellipse complète, PI : moitie Elipse etc...
  * proportion : proportion entre la grand rayon et le petit rayon
  */
   
void build_sphere(Sphere* sphere, double thetaAngle, double proportion) 
{
	/* les angles de base */
	const double theta = thetaAngle/(SPH_NBPMAX-1);
	const double phi   =    PI/(SPH_NBPMAX-1);
	
	const double hozRadius = 1;
	const double vertRadius = hozRadius * proportion ;
	int i,j;
	
	sphere->center[0] = 0;
	sphere->center[1] = 0;
	sphere->center[2] = 0;
	G3Xcolor glass  ={0.90,0.90,1.00};
	G3Xcolor metal  ={0.60,0.75,0.95};
	setColor(sphere, &glass, 0);
	setColor(sphere, &metal, 1);
	/* les sommets */
	for (i=0; i<SPH_NBPMAX; i++)
	{
		double cosiphi = cos(i*phi);
		double siniphi = sin(i*phi);
		for (j=0; j<SPH_NBPMAX; j++)
		{
			int    k =i*SPH_NBPMAX+j;
			double cosjtheta = cos(j*theta);
			double sinjtheta = sin(j*theta);
			/* coordoonées sphériques (Eq. paramétrique) */
			sphere->points[k][0]= hozRadius * cosjtheta*siniphi;
			sphere->points[k][1]= vertRadius * sinjtheta*siniphi;
			sphere->points[k][2]= vertRadius * cosiphi;
		}
	}
	/* les normales : même chose que les sommets */
	memcpy(sphere->normals, sphere->points, SPH_POINTS_NUM*sizeof(G3Xcoord));
}	
