#include "../include/Object.h"
#include "../include/rectangle.h"


 void build_rectangle(Rectangle* rectangle, int length, int width) {
 	
	float x = -((float)length)/2;
	float z = -(float)width/2 ;
	int i = 0;
	
    const float step =  (float)(length) / (RECT_LENG_MAX_POINTS_NUM-1) ;
	
	for ( i = 0; i < RECT_LENG_MAX_POINTS_NUM; x +=step, i++ ) 
	{
		G3Xpoint* vertice1 = &(rectangle->firstLongSide)[i];
		memset ( vertice1, 0, sizeof( G3Xpoint ));
		
		(*vertice1)[0] = x ;
		(*vertice1)[1] = 0 ;
		(*vertice1)[2] = -z ;
		
		G3Xpoint* vertice2 = &(rectangle->secondLongSide)[i];
		
		(*vertice2)[0] = x ;
		(*vertice2)[1] = 0 ;
		(*vertice2)[2] = z ;
													
	}
	rectangle->normal[0] = 0;
	rectangle->normal[1] = 1;
	rectangle->normal[0] = 0;
	
	
	
	for ( i = 0; i < RECT_WITDH_MAX_POINTS_NUM; z +=step, i++ ) 
	{
		G3Xpoint* vertice1 = &(rectangle->firstShortSide)[i];
		memset ( vertice1, 0, sizeof( G3Xpoint ));
		
		(*vertice1)[0] = z ;
		
		G3Xpoint* vertice2 = &(rectangle->secondShortSide)[i];
		memset ( vertice2, 0, sizeof( G3Xpoint ));
		
		(*vertice2)[0] = z ;
	}
	
  
}

void drawRectangle(Rectangle * rectangle) {
	
	int i = 0;
		
	for ( i = 0; i < RECT_LENG_MAX_POINTS_NUM - 1; i++ ) 
	{
		glBegin(GL_QUADS);
		glNormal3dv(rectangle->normal);
		
		G3Xpoint* vertice1 = &(rectangle->firstLongSide)[i];
		drawAPoint (vertice1);
		G3Xpoint* vertice2 = &(rectangle->secondLongSide)[i];
		
		drawAPoint (vertice2);
		
		
		G3Xpoint* vertice4 = &(rectangle->secondLongSide)[i+1];
		drawAPoint (vertice4);
		
		G3Xpoint* vertice3 = &(rectangle->firstLongSide)[i+1];
		drawAPoint (vertice3);
		
		
		glEnd();
	}
	
	   	
   
}

