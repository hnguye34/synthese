#include <stdio.h>
#include <g3x.h>
#include <cone.h>


void  addConeCirclePoint (Circle* circle, double x , double y, double z) {
	
	if ( circle->indice + 1 >= CONE_CIRCLE_POINTS_NUM)
	{
		printf ("-->[addConeCirclePoint] [ERROR] : indice ou of range:%d/%d", circle->indice , CONE_CIRCLE_POINTS_NUM) ;
		return;
	}
	circle->indice++;
	G3Xpoint*  vertice = &circle->points[circle->indice];
	 
	(*vertice)[0] = x;
	(*vertice)[1] = y;
	(*vertice)[2] = z;
	/*circle->normals[circle->indice][0] = x;
	circle->normals[circle->indice][0] = y;
	circle->normals[circle->indice][0] = z;*/
}

void draw_circle( Circle * circle)
{
	int i;

	
	glBegin(GL_QUAD_STRIP);
	
	for (i = 0; i < CONE_CIRCLE_POINTS_NUM  ; i++)
	{
		drawPoint (circle->points, i);		
	}
	glEnd();
	
}

void draw_level (Circle * lowCircle, Circle * upCircle, Cone * cone,  int level ) {
	
	int j= 0;
	
	glBegin(GL_QUADS);
	for (j = 0; j < CONE_CIRCLE_POINTS_NUM  ; j++)
	{
		G3Xvector* normal = &(cone->normals[level][j]);
		glNormal3dv( *normal);
		/* glNormal3dv( (G3Xvector) {1,0, 1} );*/
		drawAPoint (&lowCircle->points[j] );
		drawAPoint (&upCircle->points[j]);
		
		if ( j + 1 < CONE_CIRCLE_POINTS_NUM ) {
			drawPoint (upCircle->points, j+1);
			drawPoint (lowCircle->points, j+1);
		}
		else {
			drawPoint (upCircle->points, 0);
			drawPoint (lowCircle->points, 0);
		}
		
	}
	glEnd();
}

void drawCone( Cone * cone)
{
	int i = 0;

	/* 1 circle by level */
	for (i = 0; i  < CONE_LEVEL_NUM  ; i++) {
		
		/*draw_circle ( &(cone->circles[i])); */
		
		Circle * lowCircle = &(cone->circles[i]) ;
		Circle * upCircle =  &(cone->circles[i+1]) ;
		
		draw_level ( lowCircle, upCircle, cone, i );
		
	}
	
	 
}
	
void build_circle( Circle * circle, double y, double ray)
{

	int 		i ;
	double 		pas = 4 * PI/CONE_CIRCLE_POINTS_NUM;
	double 		a = 0.,cosa,sina;
	
	(*circle).center[0] = 0 ;
	(*circle).center[1] = y ;
	(*circle).center[2] = 0 ;
	
	for (i = 0; i < CONE_CIRCLE_POINTS_NUM ; i++)
	{
		cosa= ray*cos(a);
		sina= ray*sin(a);
		a+=pas;
		addConeCirclePoint	(circle, cosa, y, sina);
	}
}

void build_cone( Cone * cone)
{
	int i = 0;
	const float height = 3;
	const float step = height / CONE_LEVEL_NUM;
	const float lowestRay = 1 ;
	memset ( cone->top, 0, sizeof ( G3Xpoint));
	
	cone->top[1] = height;
	
	float y = 0;
	for (i = 0; i < CONE_LEVEL_NUM + 1 ; i++, y+= step) {
		double ray = (height - y)*lowestRay /  height  ; /* 4 : because the biggest (and lowest) circle is ray 2 */
		Circle * circle = &(cone->circles[i]);
		circle->indice = -1;
		build_circle (circle , y, ray);
	}
	
	/* build normal */
	int k = 0;
	for (i = 0; i  < CONE_LEVEL_NUM ; i++) {
		
		/*draw_circle ( &(cone->circles[i])); */
		
		Circle * lowCircle = &(cone->circles[i]) ;
		Circle * upCircle =  &(cone->circles[i+1]) ;
		
		int j = 0;
		
		for (j = 0; j < CONE_CIRCLE_POINTS_NUM  ; j++, k++)
		{
		
			G3Xpoint* A = &lowCircle->points[j];
			G3Xpoint* B = &upCircle->points[j];
			G3Xpoint* C = j + 1 < CONE_CIRCLE_POINTS_NUM ? &(lowCircle->points[j+1]) : &(lowCircle->points[0]) ;
			normalFromABC ( &(cone->normals[i][j]),  A , B , C, 1); 
		}
	
		
	}
	
}
