/* ===============================================================
  E.Incerti - Universit� de Marne-la-Vall�e - incerti@univ-mlv.fr
  ============================================================= */

#include <stdio.h>
#include <g3x.h>
#include "cylinder.h"

#define NBMMAX 100
#define NBPMAX 100
static GLenum affichage = GL_QUAD_STRIP;

static G3Xpoint  vertex[NBMMAX*NBPMAX];
static G3Xvector normal[NBMMAX*NBPMAX];
static G3Xcolor  colmap[NBMMAX*NBPMAX];
static int    nbm,nbp;
static double r;

static G3Xcolor marron1={0.30,0.20,0.10};
static G3Xcolor marron2={0.50,0.40,0.20};
static G3Xcolor rouge  ={1.00,0.00,0.00};
static G3Xcolor vert   ={0.00,1.00,0.00};
static G3Xcolor bleu   ={0.00,0.00,1.00};
static G3Xcolor jaune  ={1.00,1.00,0.00};
static G3Xcolor cyan   ={0.00,1.00,1.00};
static G3Xcolor orange ={0.75,0.50,0.00};
static G3Xcolor vert2  ={0.50,0.75,0.00};
static G3Xcolor metal  ={0.60,0.75,0.95};
static G3Xcolor glass  ={0.90,0.90,1.00};


void Material(G3Xcolor col, float ambi, float diff, float spec, float shine, float transp)
{
	float tmp[4];
	tmp[3]=transp;

	tmp[0]=ambi*col[0];
	tmp[1]=ambi*col[1];
	tmp[2]=ambi*col[2];
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT  ,tmp);
	tmp[0]=diff*col[0];
	tmp[1]=diff*col[1];
	tmp[2]=diff*col[2];
	glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE  ,tmp);
	tmp[0]=spec*col[0];
	tmp[1]=spec*col[1];
	tmp[2]=spec*col[2];
	glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR ,tmp);
	glMaterialf (GL_FRONT_AND_BACK,GL_SHININESS,shine*256.);
}


static void torus(int numc, int numt)
{
   int i, j, k;
   double s, t, x, y, z, twopi;

   twopi = 2 * PI;
   for (i = 0; i < numc; i++) {
      glBegin(GL_QUAD_STRIP);
     /*   glColor4f(0.0, 0.0, 1.0, 1.0);*/
      for (j = 0; j <= numt; j++) {
         for (k = 1; k >= 0; k--) {
            s = (i + k) % numc + 0.5;
            t = j % numt;

            x = (1+.1*cos(s*twopi/numc))*cos(t*twopi/numt);
            y = (1+.1*cos(s*twopi/numc))*sin(t*twopi/numt);
            z = .1 * sin(s * twopi / numc);
            glVertex3f(x, y, z);
         }
      }
      glEnd();
   }
}

static void spring(int numc, int numt, int h, int tour)
{
   int i, j, k;
   double s, t, x, y, z, twopi;
    double a = 0.5;
    double R = 5;
    double delta = 1;
   twopi = 2 * PI;
   for (i = 0; i < numc; i++) { delta = 1;
      glBegin(GL_QUAD_STRIP);
     /*   glColor4f(0.0, 0.0, 1.0, 1.0);*/
      for (j = 0; j < numt; j++) {
         for (k = 1; k >= 0; k--) {
            s = (i + k) % numc + 0.5;
            t = j % numt;
            /*double newH = newH *(1.01);*/
            x = (R+a*cos(s*twopi/numc))*cos(t*twopi/numt*tour);
            y = (R+a*cos(s*twopi/numc))*sin(t*twopi/numt*tour);
            z = a * sin(s * twopi / numc) + delta*h*t*twopi/numt/numt*tour;
            delta += 0.004;
            glVertex3f(x, y, z);
         }
      }
      glEnd();
   }
}

static void cone(int h, int r, double theta)
{
    int u;
    double x,y,z;
      glBegin(GL_QUAD_STRIP);
      for (u = 0; u < h; u++) {
            x = (h-u)/h*r*cos(theta);
            y = (h-u)/h*r*sin(theta);
            z = u;
            glVertex3f(x, y, z);

      }
      glEnd();
}


static void SphereCanonique(void)
{
	/* les angles de base */
	double theta = 2.*PI/(NBMMAX-1);
	double phi   =    PI/(NBPMAX-1);
	int i,j;
	/* les sommets */

	glBegin(GL_QUADS);

	for (i=0; i<NBPMAX; i++)
	{
		double cosiphi = cos(i*phi);
		double siniphi = sin(i*phi);
		for (j=0; j<NBMMAX; j++)
		{
			int    k =i*NBMMAX+j;
			double cosjtheta = cos(j*theta);
			double sinjtheta = sin(j*theta);
			/* coordoon�es sph�riques (Eq. param�trique) */
			vertex[k][0]= cosjtheta*siniphi;
			vertex[k][1]= sinjtheta*siniphi;
			vertex[k][2]= cosiphi;
		}
	}
	/* les normales : m�me chose que les sommets */
	memcpy(normal,vertex,NBMMAX*NBPMAX*sizeof(G3Xcoord));
}

/* dessin de la sph�re en rendu par points */
static void sphere_draw_0(int n, int p)
{
	int nstep=NBMMAX/n;
	int pstep=NBPMAX/p;
	int i,j;
	glDisable(GL_LIGHTING);
	glPointSize(4.);
	glBegin(GL_POINTS);
	for (i=0;i<NBPMAX;i+=pstep)
		for (j=0;j<NBMMAX;j+=nstep)
		{
			int k=i*NBMMAX+j;
			glColor3fv(colmap[k]);
			glVertex3dv(vertex[k]);
		}
	glEnd();
	glEnable(GL_LIGHTING);
}


static void sphere_draw_1(int n, int p)
{
	int nstep=NBMMAX/n;
	int pstep=NBPMAX/p;
	int i,j;
	glPointSize(4.);
	glBegin(GL_POINTS);
	for (i=0;i<NBPMAX;i+=pstep)
		for (j=0;j<NBMMAX;j+=nstep)
		{
			int k=i*NBMMAX+j;
			g3x_Material(colmap[k],0.25,0.5,0.5,0.5,0.);
			glNormal3dv(normal[k]);
			glVertex3dv(vertex[k]);
		}
	glEnd();
}

/* dessin de la sphere en facettes */
static void sphere_draw_2(int n, int p)
{
	int nstep=NBMMAX/n;
	int pstep=NBPMAX/p;
	int i,j,k;
	glBegin(GL_QUADS);
	for (i=0;i<NBPMAX-pstep;i+=pstep)
	{
		for (j=0;j<NBMMAX-nstep;j+=nstep)
		{
			k=(i      )*NBMMAX+j;
			g3x_Material(colmap[k],0.25,0.5,0.5,0.5,0.);
			glVertex3dv(vertex[k]);
			k=(i      )*NBMMAX+j+nstep;
			g3x_Material(colmap[k],0.25,0.5,0.5,0.5,0.);
			glVertex3dv(vertex[k]);
			k=(i+pstep)*NBMMAX+j+nstep;
			g3x_Material(colmap[k],0.25,0.5,0.5,0.5,0.);
			glVertex3dv(vertex[k]);
			k=(i+pstep)*NBMMAX+j;
			g3x_Material(colmap[k],0.25,0.5,0.5,0.5,0.);
			glVertex3dv(vertex[k]);
		}
		k=(i      )*NBMMAX+j;
		g3x_Material(colmap[k],0.25,0.5,0.5,0.5,0.);
		glVertex3dv(vertex[k]);
		k=(i      )*NBMMAX;
		g3x_Material(colmap[k],0.25,0.5,0.5,0.5,0.);
		glVertex3dv(vertex[k]);
		k=(i+pstep)*NBMMAX;
		g3x_Material(colmap[k],0.25,0.5,0.5,0.5,0.);
		glVertex3dv(vertex[k]);
		k=(i+pstep)*NBMMAX+j;
		g3x_Material(colmap[k],0.25,0.5,0.5,0.5,0.);
		glVertex3dv(vertex[k]);

	}
	glEnd();
}
/*
void gl_Cylinder(int coupe, float ray, float thinnes)
{
 	int i;
 	coupe = 20;
	double pas=2*PI/coupe;
	double a=0.,cosa,sina;

  glBegin( affichage );
  for (i=0;i <= coupe; i++)
  {
	cosa=ray* cos(a);
	sina=ray* sin(a);
  	a+=pas;
    glNormal3d(cosa, sina, 0.0);
    glVertex3d(cosa, sina,-thinnes);
    glVertex3d(cosa, sina,+thinnes);
  }

  glEnd();
}
*/

static void cube_param(int larg, int haut, int thinest)
{

  glBegin(GL_QUAD_STRIP);
 /* glBegin( affichage );*/
  int i = 0;
  int j = 0;
  int pas = 2;
  for (i = 0;i<=larg;i= i+ pas)
  /*  for (j = 0;j<=haut;j = j + pas )*/
  {

  /*  glNormal3d(i, j, 0.0);
    glVertex3d(0, 1, i);
    glVertex3d(1, 1, i);
   /* glVertex3d(j, i, -1);
    glVertex3d(i, j, - 1.0); */

       /* front */

    glVertex3f(0.0f, 0.0f, i);
    glVertex3f(1.0f, 0.0f, i);
    glVertex3f(1.0f, 1.0f, i);
    glVertex3f(0.0f, 1.0f, i);
    glNormal3d(0, 0, i);

  }

  glEnd();
}


static void cube_cylinder(int n, int p)
{
    int slices = 10;
    int i = 0;
    int radius = 10;
    int halfLength = 30;
    for(i=0; i<slices; i++) {
        float theta = ((float)i)*2.0*PI;
        float nextTheta = ((float)i+1)*2.0*PI;
        glBegin(GL_TRIANGLE_STRIP);
        glColor4f(0.0, 0.0, 1.0, 1.0);
        /*vertex at middle of end */
        glVertex3f(0.0, halfLength, 0.0);
/*vertices at edges of circle*/
        glVertex3f(radius*cos(theta), halfLength, radius*sin(theta));
        glVertex3f (radius*cos(nextTheta), halfLength, radius*sin(nextTheta));
/* the same vertices at the bottom of the cylinder*/
        glVertex3f (radius*cos(nextTheta), -halfLength, radius*sin(nextTheta));
        glVertex3f(radius*cos(theta), -halfLength, radius*sin(theta));
        glVertex3f(0.0, -halfLength, 0.0);
        glEnd();
        }
}



static void cube_by_slice(float larg, float haut, float thiness)
{
	int i,j,k;
	glBegin(GL_QUADS);

    /* front */
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(larg, 0.0f, 0.0f);
    glVertex3f(larg, haut, 0.0f);
    glVertex3f(0.0f, haut, 0.0f);

    glVertex3f(0.0f, 0.0f, -thiness);
    glVertex3f(larg, 0.0f, -thiness);
    glVertex3f(larg, haut, -thiness);
    glVertex3f(0.0f, haut, -thiness);
    /* right */
    glVertex3f(larg, 0.0f, 0.0f);
    glVertex3f(larg, 0.0f, -thiness);
    glVertex3f(larg, haut, -thiness);
    glVertex3f(larg, haut, 0.0f);
    /* left */
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, -thiness);
    glVertex3f(0.0f, haut, -thiness);
    glVertex3f(0.0f, haut, 0.0f);
    /* top */
    glVertex3f(0.0f, haut, 0.0f);
    glVertex3f(larg, haut, 0.0f);
    glVertex3f(larg, haut, -thiness);
    glVertex3f(0.0f, haut, -thiness);
    /* bottom */
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(larg, 0.0f, 0.0f);
    glVertex3f(larg, 0.0f, -thiness);
    glVertex3f(0.0f, 0.0f, -thiness);

    glEnd();
}


/* Rotate about x-axis when "x" typed; rotate about y-axis
   when "y" typed; "i" returns torus to original view */
void keyboard(unsigned char key, int x, int y)
{
   switch (key) {/*
   case 'x':
   case 'X':
        affichage = GL_TRIANGLE_STRIP;
        glutPostRedisplay();
      break;
   case 'y':
   case 'Y':

      affichage = GL_POINTS;
      glutPostRedisplay();
      break;
      /*
   case 'i':
   case 'I':
      glLoadIdentity();
      gluLookAt(0, 0, 10, 0, 0, 0, 0, 1, 0);
      glutPostRedisplay();
      break;
      */
   case 27:
      exit(0);
      break;
   }
}

void Init(void)
{
	nbm=50;
	g3x_CreateScrollv_i("nbm",&nbm,3,NBMMAX,1.,"nombre de m�ridiens ");
	nbp=50;
	g3x_CreateScrollv_i("nbp",&nbp,3,NBPMAX,1.,"nombre de parall�les");

	/* initialisation d'une carte de couleurs */
 	g3x_FillColorMap(colmap,NBMMAX*NBPMAX);
	/* construction fr la sph�re canonique */
	SphereCanonique();
}



/*
 Pour r�gler les pieds:

 - Trouner les table de facon les pieds orient� vers vous.
 - y : gacuhe/droite
 - x: hau/base
 - x : pr�s/loin de vous
*/

static void chaise(double width, double depth, double tableThickness, double legThick, double legLeng , double dScale)
{
    tableThickness = 2;
    width = 1;
    depth = 1;
    const double finition = 20;
    glPushMatrix();


            glPushMatrix();
               /* glTranslatef(0.2,0.1,1); *//* y:ok */

                Material(vert,0.25,0.5,0.,0.,1.);
                cube_by_slice(width,tableThickness, depth);
            glPopMatrix();
        /* les 4 pieds */
  glScalef(1, 1, 5);
          glPushMatrix();
            double x1 =  legThick; /* OK*/
            double y1 = dScale; /* + : droit; - : gauche OK*/
            double z1 = -1.1;
            glTranslatef(x1, y1, z1);
            gl_Cylinder(finition, legThick, legLeng);
          glPopMatrix();

          glPushMatrix();
            double x2= 0.9;
            glTranslatef( x2 , y1, z1);
            gl_Cylinder(finition, legThick, legLeng);
          glPopMatrix();

          glPushMatrix();
            double y3 = y1+2.4-0.6;
            glTranslatef(x1, y3, z1); /* y gauche/droite */
            gl_Cylinder(finition, legThick, legLeng);
          glPopMatrix();
          glPushMatrix();
            glTranslatef(x2 , y3, z1);
            gl_Cylinder(finition, legThick, legLeng);
          glPopMatrix();
    glPopMatrix();
}

static void table(double width, double depth, double tableThickness, double legThick, double legLeng)
{
    tableThickness = 2;
    width = 1;
    depth = 1;
    const double finition = 20;
    glPushMatrix();
            glPushMatrix();
               /* glTranslatef(0.2,0.1,1); *//* y:ok */
                double wScale = 1;
                double lScale = wScale * 1.3;
                double dScale = 0.1;
                glScalef(wScale, lScale, dScale);
                Material(vert,0.25,0.5,0.,0.,1.);
                cube_by_slice(width,tableThickness, depth);
            glPopMatrix();
        /* les 4 pieds */

          glPushMatrix();
            double x1 =  legThick; /* OK*/
            double y1 = dScale; /* + : droit; - : gauche OK*/
            double z1 = -1.1;
            glTranslatef(x1, y1, z1);
            gl_Cylinder(finition, legThick, legLeng);
          glPopMatrix();

          glPushMatrix();
            double x2= 0.9;
            glTranslatef( x2 , y1, z1);
            gl_Cylinder(finition, legThick, legLeng);
          glPopMatrix();

          glPushMatrix();
            double y3 = y1+2.4;
            glTranslatef(x1, y3, z1); /* y gauche/droite */
            gl_Cylinder(finition, legThick, legLeng);
          glPopMatrix();
          glPushMatrix();
            glTranslatef(x2 , y3, z1);
            gl_Cylinder(finition, legThick, legLeng);
          glPopMatrix();
    glPopMatrix();


}



static void lottable_Chaise(void)
{

  glPushMatrix();
        double attitude = -0.5;
        double horizontal = -1.2;
        double deepth = 0; /* Bizarre , ca fait bouger en en attitude aussi*/
        glTranslatef(attitude, horizontal, deepth); /* Positonner correctement la sc�ne*/
        double legThick = 0.1;
        double legLen = 1;
          /* une table */

        table(1,1,1, legThick, legLen);
    glPushMatrix();
         glTranslatef(-1.0,1.5,-1.5);
         double appleSize = 0.15;
        glScalef(appleSize, appleSize, appleSize);
        sphere_draw_2 ( 20, 20);
     glPopMatrix();

    /* 4 chaises */
    glPushMatrix();
      double wScale = 1;
      double lScale = 0.5 ;
      double dScale = 0.1;
      glTranslatef(-1.5,0.,-1);
      glScalef(wScale, lScale, dScale);
      Material(rouge,0.25,0.5,0.0,1.0,1.);
        chaise(1,1,1, legThick, legLen, dScale);
    glPopMatrix();


  glPopMatrix();
}
static void Draw2(void) {

    glPushMatrix();
        double attitude = -0.5;
        double horizontal = -1.2;
        double deepth = 0; /* Bizarre , ca fait bouger en en attitude aussi*/
        glTranslatef(attitude, horizontal, deepth); /* Positonner correctement la sc�ne*/
        double legThick = 0.1;
        double legLen = 1;
        table(1,1,1, legThick, legLen);
    glPopMatrix();
}

static void crane(G3Xcolor color) {
    double legThick = 0.1;
    double legLen = 1;
    const double finition = 20;

        g3x_Material(color,0.25,0.5,0.5,0.5,0.);
      	int nstep=NBMMAX/finition;
	int pstep=NBPMAX/finition;
	int i,j,k;
	glBegin(GL_QUADS);
	for (i=0;i<NBPMAX-pstep;i+=pstep)
	{
		for (j=0;j<NBMMAX-nstep;j+=nstep)
		{
			k=(i      )*NBMMAX+j;

			glVertex3dv(vertex[k]);
			k=(i      )*NBMMAX+j+nstep;

			glVertex3dv(vertex[k]);
			k=(i+pstep)*NBMMAX+j+nstep;

			glVertex3dv(vertex[k]);
			k=(i+pstep)*NBMMAX+j;

			glVertex3dv(vertex[k]);
		}
		k=(i      )*NBMMAX+j;

		glVertex3dv(vertex[k]);
		k=(i      )*NBMMAX;

		glVertex3dv(vertex[k]);
		k=(i+pstep)*NBMMAX;

		glVertex3dv(vertex[k]);
		k=(i+pstep)*NBMMAX+j;

		glVertex3dv(vertex[k]);

	}
	glEnd();


}

static void head() {

    double headSize =  0.5;
    double eyesSize =  headSize/3;
    double couSize =  headSize/1;
    glPushMatrix();
        glScalef(headSize, headSize, headSize);
        crane(vert);
        Material(vert,0.25,0.5,0.,0.,0.7);
     glPushMatrix();
        glTranslatef(0, 0.8, 0.8); /* Positonner correctement la sc�ne*/

        glScalef(eyesSize, eyesSize, eyesSize);
        crane(rouge);
    glPopMatrix();
        /*glScalef(couSize, couSize, couSize);*/
        glTranslatef(-0.2, -0.2, -1); /* Positonner correctement la sc�ne*/
      /*  glRotatef (PI/2, 1,  1, 1);*/

    	glRotatef(200,1.,0.,0.);
        gl_Cylinder(5, 0.45, 0.38 );
    glPopMatrix();
}
static void Draw(void) {

  double attitude = -0.5;
  double horizontal = -1.2;
  double deepth = 0; /* Bizarre , ca fait bouger en en attitude aussi*/
  glTranslatef(attitude, horizontal, deepth); /* Positonner correctement la sc�ne*/

    head();
}


int main(int argc, char** argv)
{
  /* initialisation de la fen�tre graphique et param�trage Gl */
  g3x_InitWindow(*argv,768,512);

	/* param�tres cam�ra */
  /* param. g�om�trique de la cam�ra. cf. gluLookAt(...) */
  g3x_SetPerspective(40.,100.,1.);
  /* position, orientation de la cam�ra */
  g3x_SetCameraSpheric(0.25*PI,+0.25*PI,6.,(G3Xpoint){0.,0.,0.});

  /* d�finition des fonctions */
  g3x_SetInitFunction(Init);     /* la fonction d'initialisation */
  g3x_SetDrawFunction(Draw);     /* la fonction de dessin        */
	g3x_SetAnimFunction(NULL);		 /* pas de fonction d'animation  */
  g3x_SetExitFunction(NULL);     /* pas de fonction de sortie    */
/*glutKeyboardFunc(keyboard);*/
  /* boucle d'ex�cution principale */
  return g3x_MainStart();
  /* rien apr�s �a */
}
