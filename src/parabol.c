#include "../include/Object.h"
#include "../include/parabol.h"
#include <g3x_basix.h>


 void build_parabol(Parabol* parabol) {
 	
	int i = 0;
	int p = 1 ;
	double t = -3;
	const double step = 0.2;
	for ( ; i < PAR_POINTS_NUM; i++, t+=step ) {
		double x =  t;
		double y =  0.5* t * t ;
		
		G3Xset( parabol->points[i] , x , y, 0);
	}
	
}




void drawParabol(Parabol * parabol) {
	int i = 0;
	glBegin(GL_LINE_STRIP);
    
    for (; i < PAR_POINTS_NUM; i++) {
  		 drawAPoint (&parabol->points[i]);		
	}
	
    glEnd();  
    
}

