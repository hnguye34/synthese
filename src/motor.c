#include <stdio.h>
#include <g3x.h>
#include <Object.h>
#include <motor.h>


void drawMotor( Motor * motor)
{
	g3x_Material(motor->color,0.25,0.5,0.,0.,1.);
		
	drawCylinder (&motor->engine); 
	int i = 0;
	for ( i = 0; i < MOTOR_BLADES_NUM; i++) {
		drawQuadrilateral (&motor->blades[i]); 
	}
}


void build_blades (Motor * motor, double thickness )
{
	double width = 0.5;
	double frontSide = 0.2;
	double behindSide = 1.0;
	int i = 0;	
	for ( i = 0; i < MOTOR_BLADES_NUM; i++) {
		build_quadrilateral (&motor->blades[i], width, frontSide, behindSide,  PI/2, PI/2, thickness); 
	}

}

void build_motor(Motor * motor,  G3Xcolor		color)
{
	const double engineRadius = 0.7; 
	const double engineLen = engineRadius * 2.2; 
	
	copyColor(motor->color,  color) ;

	build_cylinder (&motor->engine); 
	rotate_cylinder (&motor->engine, PI/2, AXIS_X); 
	scale_cylinder ( &motor->engine, engineRadius, engineLen, engineRadius);
	
	setHead1Height ( &motor->engine, 1.6);
	
	double thickness = 0.1;
	build_blades(motor, thickness );
	
	rotate_quadrilateral (&motor->blades[1], PI/2, AXIS_Y);
	rotate_quadrilateral (&motor->blades[2], PI, AXIS_Y);
	rotate_quadrilateral (&motor->blades[3], 3*PI/2, AXIS_Y);
	
	/*int i = 0;
	for ( i = 0; i < MOTOR_BLADES_NUM; i++) {
		scale_quadrilateral (&motor->blades[i], 2, 1, 1); 
	}*/
	
	translate_quadrilateral(&motor->blades[0], engineRadius, -engineLen , 0.0 ); 
	translate_quadrilateral(&motor->blades[1], 0.0, -engineLen , -engineRadius); 
	translate_quadrilateral(&motor->blades[2], -engineRadius, -engineLen, 0.0 ); 
	translate_quadrilateral(&motor->blades[3], 0.0,  -engineLen , engineRadius ); 
	
}	


void translate_motor(Motor* motor, double x, double y, double z ) {

	int i = 0;
	
	for ( i = 0; i < MOTOR_BLADES_NUM; i++) {
		translate_quadrilateral (&motor->blades[i], x, y, z); 
	}
	
	translate_cylinder ( &motor->engine, x, y, z) ;
	
}

void scale_motor(Motor* motor, double x, double y, double z ) {

	int i = 0;
	
	for ( i = 0; i < MOTOR_BLADES_NUM; i++) {
		scale_quadrilateral (&motor->blades[i], x, y, z); 
	}
	
	scale_cylinder ( &motor->engine, x, y, z) ;
	
}


void rotate_motor(Motor* motor, double angle, AXIS axis) {
	
	int i = 0;
	/*dumpPoint (&motor->engine.face1Center, "face1Center");
	dumpPoint (&motor->engine.face2Center, "face2Center");
	dumpPoint (&motor->engine.face1Points[0], "Points sur le cercle");*/
	for ( i = 0; i < MOTOR_BLADES_NUM; i++) {
		/*printf("-->[rotate_motor] %d /%d \n", i, axis );*/
		rotate_quadrilateral (&( motor->blades[i]), angle, axis); 
	}
	
	rotate_cylinder( &motor->engine, angle, axis) ;
}

void start_motor ( Motor* motor, double speed, AXIS axis )
{
	int i = 0;
	for ( i = 0; i < MOTOR_BLADES_NUM; i++) {
	/*	printf("-->[start_motor] %d /%d \n", i, axis );
		dumpPoint (& motor->blades[i].face1.D);*/
		double x = motor->engine.face1Center[0];
		double y = motor->engine.face1Points[0][1];
		double z = motor->engine.face1Center[2];
		
		translate_quadrilateral(& motor->blades[i], -x,-y, -z );
		rotate_quadrilateral (&( motor->blades[i]), speed, axis);
		translate_quadrilateral(& motor->blades[i], x, y, z );
		/*
		printf ("APRES rot\n");
		dumpPoint(& motor->blades[i].face1.D);*/
	}
}
