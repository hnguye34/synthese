#include "../include/Object.h"
#include "../include/cube.h"
#include <g3x_basix.h>


 void build_cube(Cube* cube) {
 	
	G3Xcamera * cam = g3x_GetCamera();
	/*double dCamVisee = g3x_GetCameraDist();*/
	
	cube->initialCamDist = cam->dist;
	/*g3x_GetCameraDist();*/
	/*printf ("===> build_cube: Dist Cam-Visee =%f . Point att:(%f ,%f, %f). Point visee:(%f ,%f, %f).\n",
			cam->dist, cam->pos[0], cam->pos[1], cam->pos[2] , cam->tar[0], cam->tar[1], cam->tar[2]  ) ;*/
	const float extremity = 1 ;
	const float step = extremity * 2 / SLICE_NUM  ;
	float curX = 0;
	cube->curPosition = -1;
	cube->xNormal[0] =1 ;
	cube->xNormal[1] =0 ;
	cube->xNormal[2] =0 ;

	cube->yNormal[0] =0 ;
	cube->yNormal[1] =1 ;
	cube->yNormal[2] =0 ;

	cube->zNormal[0] =0 ;
	cube->zNormal[1] =0 ;
	cube->zNormal[2] =1 ;
	
	float curPos = 0  ;
	int count = 0;
	
	for (count = 0, curPos = -extremity; count < POINT_NUM; curPos+=step, count++)
	{
		CubeFace * face 	= &cube->faces[0] ;
		CubeFace * behind 	= &cube->faces[1];
		CubeFace * left 	= &cube->faces[2] ;
		CubeFace * right 	= &cube->faces[3];

		G3Xset(left->points[0][count], 	-extremity, extremity, 	curPos  );
		G3Xset(left->points[1][count], 	-extremity, -extremity, curPos );
		setVct(&left->normal, 1, 0, 0);
	
			
		G3Xset(face->points[0][count], 	curPos, extremity, 		extremity  );
		G3Xset(face->points[1][count], 	curPos, -extremity, 	extremity );
		setVct(&face->normal, 0, 0, 1);
		
		G3Xset(behind->points[0][count], 	curPos, extremity, 	-extremity  );
		G3Xset(behind->points[1][count], 	curPos, -extremity, -extremity );
		setVct(&behind->normal, 0, 0, -1);
		
		
	
		G3Xset(right->points[0][count], 	extremity, extremity, 	curPos  );
		G3Xset(right->points[1][count], 	extremity, -extremity, 	curPos );
		setVct(&right->normal, 1, 0, 0);
	}
	
}


void drawFrom2Side (G3Xpoint	face[] , G3Xpoint	behind[]  , int step ,G3Xvector* normal )
{
	int i = 0;
	G3Xpoint* point1 = NULL;
	G3Xpoint* point2 = NULL;
	G3Xpoint* point3 = NULL;
	G3Xpoint* point4 = NULL;

	glBegin(GL_QUADS);
	
	glNormal3dv( *normal);
   	
	for (i = 0; i+step < POINT_NUM; i+=step)
	{
		point1 = &face[i] ;
		point2 = &behind[i] ;
		point3 = &behind[i+step] ;
		point4 = &face[i+step] ;
		
		glVertex3f( (*point1)[0], (*point1)[1], (*point1)[2]) ;
		glVertex3f( (*point2)[0], (*point2)[1], (*point2)[2]) ;
		glVertex3f( (*point3)[0], (*point3)[1], (*point3)[2]) ;
		glVertex3f( (*point4)[0], (*point4)[1], (*point4)[2]) ;
	}
	 	 
	if ( i < POINT_NUM	-1 ) {
		
	
		
		point3 = &behind[POINT_NUM -1] ;
		point4 = &face[POINT_NUM -1] ;
		
		glVertex3f( (*point1)[0], (*point1)[1], (*point1)[2]) ;
		glVertex3f( (*point2)[0], (*point2)[1], (*point2)[2]) ;
		glVertex3f( (*point3)[0], (*point3)[1], (*point3)[2]) ;
		glVertex3f( (*point4)[0], (*point4)[1], (*point4)[2]) ;
	}
	
    glEnd(); 
}


void drawFace (CubeFace *face, int step )
{
	drawFrom2Side (face->points[0], face->points[1], step, &face->normal);
	return; 
}


void drawCube(Cube * object) {
	
	
	const G3Xcamera * cam = g3x_GetCamera();
	const float extremity = 1 ;
	
	int i = 0;
	
	int count = 0 ;   

	int step = 1;
	if ( object->initialCamDist*6 <  cam->dist ) {
		step = 12;
	/*	printf ("---> inital=%f , actuel =%f => Step =%d. \n", object->initialCamDist, cam->dist, step);*/
	}
	if ( object->initialCamDist*3 <  cam->dist ) {
		step = 10;
	/*	printf ("---> inital=%f , actuel =%f => Step =%d. \n", object->initialCamDist, cam->dist, step);*/
	}
	if ( object->initialCamDist*2 <  cam->dist ) {
		step = 9;
	/*	printf ("---> inital=%f , actuel =%f => Step =%d. \n", object->initialCamDist, cam->dist, step);*/
	}
	else if ( object->initialCamDist*1.5 <  cam->dist ) {
		step = 8;
	/*	printf ("---> inital=%f , actuel =%f => Step =%d. \n", object->initialCamDist, cam->dist, step);*/
	}
	else if ( object->initialCamDist*1.2 <  cam->dist ) {
		step = 7;
	/*	printf ("---> inital=%f , actuel =%f => Step =%d. \n", object->initialCamDist, cam->dist, step);*/
	}
	else if ( object->initialCamDist <  cam->dist ) {
		step = 6;
	/*	printf ("---> inital=%f , actuel =%f => Step =%d. \n", object->initialCamDist, cam->dist, step);*/
	}
	else  if ( object->initialCamDist *0.9 <  cam->dist)
	{
		step = 5;
	/*	printf ("---> Inital=%f , actuel =%f => Step =%d \n", object->initialCamDist, cam->dist, step);*/
	}
	else  if ( object->initialCamDist *0.8 <  cam->dist)
	{
		step = 4;
	/*	printf ("---> Inital=%f , actuel =%f => Step =%d \n", object->initialCamDist, cam->dist, step);*/
	}
	else  if ( object->initialCamDist *0.6 <  cam->dist)
	{
		step = 3;
	/*	printf ("---> Inital=%f , actuel =%f => Step =%d \n", object->initialCamDist, cam->dist, step);*/
	}
	else  if ( object->initialCamDist *0.5 <  cam->dist)
	{
		step = 2;
	/*	printf ("---> Inital=%f , actuel =%f => Step =%d \n", object->initialCamDist, cam->dist, step);*/
	}
	else  if ( object->initialCamDist *0.3 <  cam->dist) 
	{
		step = 1 ;
		/*printf ("---> inital=%f , actuel =%f => Step =%d \n", object->initialCamDist, cam->dist, step);*/
	}
	G3Xvector topNormal = {0, 1, 0 };
	G3Xvector lowNormal = {0, -1, 0 };
	drawFrom2Side ( object->faces[0].points[0], object->faces[1].points[0] , step, &topNormal) ;
	drawFrom2Side ( object->faces[0].points[1], object->faces[1].points[1] , step, &lowNormal) ;
 
    for (count = 0; count < 4; count++) {
  		 drawFace(&object->faces[count], step );
	}
       
    
}

