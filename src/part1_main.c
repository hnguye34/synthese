#include <stdio.h>
#include <g3x.h>
#include "Object.h"
#include "cylinder.h"
#include "cube.h"
#include "cone.h"
#include "tore.h"
#include "spring.h"
#include "tetrahedron.h"
#include "rectangle.h"
#include "parabol.h"
#include "sphere.h"
#include "quadrilateral.h"
#define NBMMAX 100
#define NBPMAX 100
static GLenum affichage = GL_QUAD_STRIP;

static G3Xpoint  vertex[NBMMAX*NBPMAX];
static G3Xvector normal[NBMMAX*NBPMAX];
static G3Xcolor  colmap[NBMMAX*NBPMAX];
static int    nbm,nbp;
static double r;

static G3Xcolor marron1={0.30,0.20,0.10};
static G3Xcolor marron2={0.50,0.40,0.20};
static G3Xcolor rouge  ={1.00,0.00,0.00};
static G3Xcolor vert   ={0.00,1.00,0.00};
static G3Xcolor bleu   ={0.00,0.00,1.00};
static G3Xcolor jaune  ={1.00,1.00,0.00};
static G3Xcolor cyan   ={0.00,1.00,1.00};
static G3Xcolor orange ={0.75,0.50,0.00};
static G3Xcolor vert2  ={0.50,0.75,0.00};
static G3Xcolor metal  ={0.60,0.75,0.95};
static G3Xcolor glass  ={0.90,0.90,1.00};

static Cube cube ;
static Cylinder cylinder ;
static Cone cone ;
static Tore tore ;
static Spring spring ;
static Tetrahedron tetrahedron;
static Rectangle rectangle;
static Parabol parabol;
static Sphere sphere;
static Quadrilateral quadrilateral;
void Material(G3Xcolor col, float ambi, float diff, float spec, float shine, float transp)
{
	float tmp[4];
	tmp[3]=transp;

	tmp[0]=ambi*col[0];
	tmp[1]=ambi*col[1];
	tmp[2]=ambi*col[2];
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT  ,tmp);
	tmp[0]=diff*col[0];
	tmp[1]=diff*col[1];
	tmp[2]=diff*col[2];
	glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE  ,tmp);
	tmp[0]=spec*col[0];
	tmp[1]=spec*col[1];
	tmp[2]=spec*col[2];
	glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR ,tmp);
	glMaterialf (GL_FRONT_AND_BACK,GL_SHININESS,shine*256.);
}

void Init(void)
{
	nbm=50;
	g3x_CreateScrollv_i("nbm",&nbm,3,NBMMAX,1.,"nombre de m�ridiens ");
	nbp=50;
	g3x_CreateScrollv_i("nbp",&nbp,3,NBPMAX,1.,"nombre de parall�les");

	/* initialisation d'une carte de couleurs */
 	g3x_FillColorMap(colmap,NBMMAX*NBPMAX);
   build_cylinder(&cylinder);
   build_cube(&cube);
   build_cone(&cone);
   build_tore(&tore);
   build_spring(&spring);
   
	 
   build_tetrahedron(&tetrahedron, PI/7);     
   build_rectangle(&rectangle, 2, 1);    
   build_parabol(&parabol);    
   build_sphere(&sphere, 2*PI, 0.3);   
   double AC = 1;
	double AB = 3;
	double CD = 2.5;
	double BAC = 2*PI/3;
	double ACD = PI / 3;
	double thickness = 1;
   build_quadrilateral (&quadrilateral, AC, AB, CD,  BAC, ACD, thickness); 
   setquadriThickness1  (&quadrilateral, 1.0);
   setquadriThickness2  (&quadrilateral, 2.0);
}


static void Draw(void) {

 	glPushMatrix();
		glTranslatef(-2.5, 0, 0); /* Positonner correctement la sc�ne*/
 		glScalef(0.4, 0.4, 0.4);
 		Material(cyan,0.25,0.5,0.,0.,1.);
		drawCylinder(&cylinder);
	 		
  		glPushMatrix();
			
			glTranslatef(2.5, 0, 0); 
			Material(vert,0.25,0.5,0.,0.,1.);
			drawCone(&cone);
  			
			
			glTranslatef(2.5, 0, 0);
			Material(metal,0.25,0.5,0.,0.,1.);
			drawCube(&cube);
			glPushMatrix();
				glTranslatef(0, 0, 2); 
				Material(vert,0.25,0.5,0.,0.,1.);
				drawSphere(&sphere);
			glPopMatrix();
			glTranslatef(2.5, 0, 0); 
			Material(glass,0.25,0.5,0.,0.,1.);
			drawTore(&tore);
			
			glPushMatrix();
				glTranslatef(2.5, 0, -2); 
				Material(rouge,0.25,0.5,0.,0.,1.);
				drawSpring(&spring);
			glPopMatrix();
			
			glTranslatef(4.5, 0, 0); /* remonter un peu le pyramide */
			Material(jaune,0.25,0.5,0.,0.,1.);
			drawTetrahedron(&tetrahedron);
			
		/*	glTranslatef(1, 0, 0); 
			Material(jaune,0.25,0.5,0.,0.,1.);
			drawParabol(&parabol);
			*/
		
			
			glTranslatef(-7, -3, -3); 
			Material(vert,0.25,0.5,0.,0.,1.);
			drawQuadrilateral(&quadrilateral);
			
		glPopMatrix();			
			/*glTranslatef(2.5, 0, -2); 
			Material(rouge,0.25,0.5,0.,0.,1.);
			drawRectangle(&rectangle);
			*/
						
	glPopMatrix();
}
	

int main(int argc, char** argv)
{
  /* initialisation de la fen�tre graphique et param�trage Gl */
  g3x_InitWindow(*argv,800*2.5,600*2*5);

	/* param�tres cam�ra */
  /* param. g�om�trique de la cam�ra. cf. gluLookAt(...) */
  g3x_SetPerspective(40.,100.,1.);
  /* position, orientation de la cam�ra */
  g3x_SetCameraSpheric(0, +PI/7, 6.,(G3Xpoint){0.,0.,0.});

  /* d�finition des fonctions */
  g3x_SetInitFunction(Init);     /* la fonction d'initialisation */
  g3x_SetDrawFunction(Draw);     /* la fonction de dessin        */
	g3x_SetAnimFunction(NULL);		 /* pas de fonction d'animation  */
  g3x_SetExitFunction(NULL);     /* pas de fonction de sortie    */
/*glutKeyboardFunc(keyboard);*/
  /* boucle d'ex�cution principale */
  return g3x_MainStart();
  /* rien apr�s �a */
}
