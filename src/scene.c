#include <stdio.h>
#include <time.h>

#include <g3x.h>

#include "flight.h"
#include "dome.h"

#define NBPMAX 100
#define NBMMAX 100
static GLenum affichage = GL_QUAD_STRIP;

static G3Xcolor  colmap[NBMMAX*NBPMAX];
static int    nbm,nbp;


static G3Xcolor marron1={0.30,0.20,0.10};
static G3Xcolor marron2={0.50,0.40,0.20};
static G3Xcolor rouge  ={1.00,0.00,0.00};
static G3Xcolor vert   ={0.00,1.00,0.00};
static G3Xcolor bleu   ={0.00,0.00,1.00};
static G3Xcolor jaune  ={1.00,1.00,0.00};
static G3Xcolor cyan   ={0.00,1.00,1.00};
static G3Xcolor orange ={0.75,0.50,0.00};
static G3Xcolor vert2  ={0.50,0.75,0.00};
static G3Xcolor metal  ={0.60,0.75,0.95};
static G3Xcolor glass  ={0.90,0.90,1.00};

static Flight flight;
static Sphere sphere;
static Sphere sun;

bool avance = false; 

void Material(G3Xcolor col, float ambi, float diff, float spec, float shine, float transp)
{
	float tmp[4];
	tmp[3]=transp;

	tmp[0]=ambi*col[0];
	tmp[1]=ambi*col[1];
	tmp[2]=ambi*col[2];
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT  ,tmp);
	tmp[0]=diff*col[0];
	tmp[1]=diff*col[1];
	tmp[2]=diff*col[2];
	glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE  ,tmp);
	tmp[0]=spec*col[0];
	tmp[1]=spec*col[1];
	tmp[2]=spec*col[2];
	glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR ,tmp);
	glMaterialf (GL_FRONT_AND_BACK,GL_SHININESS,shine*256.);
}

void Init(void)
{
	nbm=50;
	g3x_CreateScrollv_i("nbm",&nbm,3,NBMMAX,1.,"nombre de m�ridiens ");
	nbp=50;
	g3x_CreateScrollv_i("nbp",&nbp,3,NBPMAX,1.,"nombre de parall�les");

	/* initialisation d'une carte de couleurs */
 	g3x_FillColorMap(colmap,NBMMAX*NBPMAX);
   build_flight(&flight, glass, cyan , jaune );
   build_sphere(&sphere, 2*PI, 1); 
   build_sphere(&sun, 2*PI, 1);   
   setColor(&sun, &rouge,0);
   setColor(&sun, &rouge,1);
   
   scale_flight(&flight, 0.2, 0.2, 0.2);
   scale_sphere(&sphere, 0.4, 0.4, 0.4);
    
   G3Xpoint flightCenter ;
   copyPoint (&flightCenter, &flight.cockpit.center);  
   translate_sphere(&sun, -sun.center[0] , -sun.center[1], -sun.center[2]);
 
   translate_sphere(&sphere, -sphere.center[0] + 5, -sphere.center[1], -sphere.center[2]);
   translate_flight(&flight, -sphere.center[0], -sphere.center[1], -sphere.center[2]);
    
   rotate_flight (&flight, -PI - PI/10   , AXIS_X);
   rotate_flight (&flight, PI, AXIS_Y);
  
   translate_flight(&flight, 2.0, 0.0, 0.0);
   rotate_sphere (&sphere, PI/2, AXIS_X);
  
   /*translate_flight(&flight,  flightCenter[0], flightCenter[1], flightCenter[2] );
  */
   
	printf ("---> DEBUT: Sphere (%f,%f,%f) ,  flight (%f,%f,%f)\n" ,
    sphere.center[0], sphere.center[1], sphere.center[2],
    flight.cockpit.center[0], flight.cockpit.center[1], flight.cockpit.center[2]
    
    ); 
   }

double x, y, z;

clock_t t1 ;
double motor_speed = 0;
int tmp = 10;
const double sph_move_speead = PI/8000;
static void Draw(void) {

	double x = sphere.center[0];
	double y = sphere.center[1];
	double z = sphere.center[2];
	
	if ( avance ) {
		
		/* TODO : touner sur lui meme*/
		/* Recuperer axe de deux center du clyindre cockpet*/
		/* faire rotation avec g3x_AxeRadRot*/
		
		/* touner autour de globe */
		
	/*	printf ("---> AVANT TOURNER: Sphere (%f,%f,%f) ,  flight (%f,%f,%f)\n" ,
			sphere.center[0], sphere.center[1], sphere.center[2],
			flight.cockpit.center[0], flight.cockpit.center[1], flight.cockpit.center[2]);
		*/	
		translate_flight(&flight,  -x, -y, -z );
		  
		  /*printf ("---> TRANSLATE(%f,%f,%f): Sphere (%f,%f,%f) ,  flight (%f,%f,%f)\n" , -x, -y, -z,
			sphere.center[0], sphere.center[1], sphere.center[2],
			flight.cockpit.center[0], flight.cockpit.center[1], flight.cockpit.center[2]);
  	*/
  	
		rotate_flight (&flight, PI/20, AXIS_Z);
		translate_flight(&flight,  x, y, z );
		
	/*	  printf ("---> ROTATION: Sphere (%f,%f,%f) ,  flight (%f,%f,%f)\n" ,
			sphere.center[0], sphere.center[1], sphere.center[2],
			flight.cockpit.center[0], flight.cockpit.center[1], flight.cockpit.center[2]);
  	*/
	}
	
	drawFlight(&flight);
	start_flight (&flight, motor_speed);

	translate_sphere(&sphere,  -x, -y, -z );
	rotate_sphere (&sphere, sph_move_speead, AXIS_Z);
	translate_sphere(&sphere,  x, y, z );
	rotate_sphere (&sphere, sph_move_speead, AXIS_Z);
	drawSphere(&sphere);
	drawSphere(&sun);
  		
}

long timediffInMsecond(clock_t t1, clock_t t2) {
    long elapsed = (( t2 - t1) / CLOCKS_PER_SEC)* 1000 ;
	return elapsed;
}

const int tour_duration = 10000 ; /* second)*/
const double speed =  2 * PI / 10000 ; /* 20 s le tour*/ 
double tempo = 20 ;

static void Anim(void){
	tempo++ ;
	avance = fmod (tempo, 40 ) == 0 ;

	motor_speed =0.5;
	if (motor_speed >=360)
		motor_speed = 0;
		
	g3x_Continue();
	
}

int main(int argc, char** argv)
{

  /* initialisation de la fen�tre graphique et param�trage Gl */
  g3x_InitWindow(*argv,800*2.5,600*2*5);

	/* param�tres cam�ra */
  /* param. g�om�trique de la cam�ra. cf. gluLookAt(...) */
  g3x_SetPerspective(40.,100.,1.);
  /* position, orientation de la cam�ra */
/*  se mettre face, 45% au dessus � l'�cran */
 g3x_SetCameraSpheric(-PI/2 , PI/6, 10.,(G3Xpoint){0.,0.,0.});
  /* d�finition des fonctions */
  g3x_SetInitFunction(Init);     /* la fonction d'initialisation */
  g3x_SetDrawFunction(Draw);     /* la fonction de dessin        */
   g3x_SetAnimFunction(Anim);
   
  /* boucle d'ex�cution principale */
  return g3x_MainStart();
  /* rien apr�s �a */
}
