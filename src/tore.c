#include <stdio.h>
#include <g3x.h>
#include <tore.h>


void drawTore( Tore * tore)
{
	int i;


	glBegin(GL_QUAD_STRIP);

	for (i = 0; i < TORE_POINT_NUM  ; i++)
	{
		G3Xpoint*  point = &tore->points[i];
		drawPoint (tore->points, i);
		glNormal3d( (*point)[0], (*point)[1], (*point)[2]);
	}

	glEnd();

 }


	
void build_tore( Tore * tore)
{

   int i, j, k;
   double s, t, x, y, z, twopi = 2 * PI;
   int pointNum = 0;
   
   for (i = 0; i < TORE_POINTS_NUM_BY_CIRCLE; i++) {
      for (j = 0; j < TORE_CIRCLE_NUM; j++) {
         for (k = 1; k >= 0; k--, pointNum++) {
            s = (i + k) % TORE_POINTS_NUM_BY_CIRCLE + 0.5;
            t = j % TORE_CIRCLE_NUM;
			float v = s*twopi/TORE_POINTS_NUM_BY_CIRCLE;
			float u = t*twopi/TORE_CIRCLE_NUM;
			float q = (1+.1*cos(v)) ;
			
            x = q *cos(u);
            y = q *sin(u);
            z = .1 * sin(s * twopi / TORE_POINTS_NUM_BY_CIRCLE)  ;
          
			if ( pointNum >= TORE_POINT_NUM ) {
				printf ("-->ERROR: index out of range %d/%d/i=%d/j=%d/k=%d\n", pointNum , TORE_POINT_NUM, i , j , k );
				break;
			}
			G3Xpoint*  vertice = &tore->points[pointNum];
		 
			(*vertice)[0] = x;
			(*vertice)[1] = y;
			(*vertice)[2] = z;
         }
      }
    
   }
  
}
