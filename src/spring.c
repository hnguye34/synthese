#include <stdio.h>
#include <g3x.h>
#include <spring.h>



/*
 * formula used: https://en.wikipedia.org/wiki/Spring_(mathematics)
 */

void  buildGenericSpring(  int h, int tour, Spring * spring)
{
	
	int i, j, k;
	const float r = SPRING_CIRLCE_RAY;
	const float R = 1;
	float delta = 1;
	const twopi = 2 * PI;
	int wireNum = 0 ;
	
	for (i = 0; i < SPRING_WIRES_NUM; i++) { 
		delta = 1;
		
		Wire * wire = &(spring->wires [i]);
		
	
		int pointsNum = 0;
		for (j = 0; j < SPRING_CIRCLE_NUM ; j++) {
		 for (k = 1; k >= 0; k--, pointsNum++) {
			 
			float s = (i + k) % SPRING_WIRES_NUM + 0.5;
			float t = j % SPRING_CIRCLE_NUM;
			
			float u = s*twopi/SPRING_WIRES_NUM;
			
			float x = (R+r*cos(u))*cos(t*twopi/SPRING_CIRCLE_NUM*tour);
			float y = (R+r*cos(u))*sin(t*twopi/SPRING_CIRCLE_NUM*tour);
			float z = r * sin(u) + delta*h*t*twopi/SPRING_CIRCLE_NUM/SPRING_CIRCLE_NUM*tour;
			
			delta += SPRING_ELASTIC_DEGREE;
			

if ( pointsNum >= SPRING_CIRCLE_NUM*2 )
{
	printf (" --> ERROR buildGenericSpring: out of rangce %d / %d" ,  pointsNum, SPRING_WIRES_NUM*2);
	exit(1);
}
			G3Xpoint*  vertice = &(wire->points [pointsNum]);
			
			if ( vertice == NULL )
			{
				printf ("-->[drawGenericSpring] [ERROR] : POINTER NULL :%d\n", pointsNum) ;
				return;
				
			}
			
			(*vertice)[0] = x;
			(*vertice)[1] = y;
			(*vertice)[2] = z;
			
		 }
		}
	}
}


void  build_spring(Spring* spring)
{
	memset ( &(spring->wires), 0, SPRING_WIRES_NUM*sizeof(Wire));
	buildGenericSpring ( SPRING_LENGTH , SPRING_TOUR_NUM, spring);
}

void  drawSpring( Spring * spring )
{
	int i = 0;
	G3Xcamera * cam = g3x_GetCamera();
	/*double dCamVisee = g3x_GetCameraDist();*/
	
	
	/*g3x_GetCameraDist();*/
	
	for (i = 0; i < SPRING_WIRES_NUM; i++) { 
		glBegin(GL_QUAD_STRIP);
		
		int wireNum = 0;

		Wire * wire = &(spring->wires [i]);
		
		for ( wireNum = 0; wireNum < SPRING_CIRCLE_NUM * 2; wireNum++) { 
				
			G3Xpoint*  vertice = &(wire->points [wireNum]);
			
			glVertex3f((*vertice)[0] , (*vertice)[1], (*vertice)[2] );

		}
		
		glEnd(); 	
	}
	
	
}


static void spring(int numc, int numt, int h, int tour)
{
   int i, j, k;
   double s, t, x, y, z, twopi;
    double a = 0.5;
    double R = 5;
    double delta = 1;
   twopi = 2 * PI;
   for (i = 0; i < numc; i++) { delta = 1;
      glBegin(GL_QUAD_STRIP);
     /*   glColor4f(0.0, 0.0, 1.0, 1.0);*/
      for (j = 0; j < numt; j++) {
         for (k = 1; k >= 0; k--) {
            s = (i + k) % numc + 0.5;
            t = j % numt;
            /*double newH = newH *(1.01);*/
            x = (R+a*cos(s*twopi/numc))*cos(t*twopi/numt*tour);
            y = (R+a*cos(s*twopi/numc))*sin(t*twopi/numt*tour);
            z = a * sin(s * twopi / numc) + delta*h*t*twopi/numt/numt*tour;
            delta += 0.004;
            glVertex3f(x, y, z);
         }
      }
      glEnd();
   }
}
