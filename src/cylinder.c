#include <stdio.h>
#include <g3x.h>
#include <cylinder.h>
#include <Object.h>


void rotate_cylinder(Cylinder * cylinder, double angle, AXIS axis ) {
	rotate(cylinder->face1Points, CYL_SIDE_POINT_NUM, cylinder->sideNormals, CYL_SIDE_POINT_NUM, angle, axis);
	rotate(cylinder->face2Points, CYL_SIDE_POINT_NUM, NULL, 0, angle, axis);
	rotate(&cylinder->face1Center, 1, NULL, 0, angle, axis);
	rotate(&cylinder->face2Center, 1, NULL, 0, angle, axis);
	rotate(&cylinder->center, 1, NULL, 0, angle, axis);
	
}

void scale_cylinder(Cylinder * cylinder, double x, double y, double z ) {
	scale(cylinder->face1Points, CYL_SIDE_POINT_NUM, cylinder->sideNormals, CYL_SIDE_POINT_NUM, x, y, z);
	scale(cylinder->face2Points, CYL_SIDE_POINT_NUM, cylinder->sideNormals, CYL_SIDE_POINT_NUM, x, y, z);
	scale(&cylinder->face1Center, 1,  NULL, 0, x, y, z);
	scale(&cylinder->face2Center, 1,  NULL, 0, x, y, z);
	scale(&cylinder->center, 1,  NULL, 0, x, y, z);
}


void translate_cylinder(Cylinder * cylinder, double x, double y, double z ) {
	translate(cylinder->face1Points, CYL_SIDE_POINT_NUM, x, y, z);
	translate(cylinder->face2Points, CYL_SIDE_POINT_NUM, x, y, z);
	translate(&cylinder->face1Center, 1,  x, y, z);
	translate(&cylinder->face2Center, 1,  x, y, z);
	translate(&cylinder->center, 1,   x, y, z);
}


void setHead1Height(Cylinder * cylinder,double height) {
	scale(&cylinder->face1Center, 1, cylinder->face1Normals, CYL_SIDE_POINT_NUM, 1.0, height, 1.0);
}

void setHead2Height(Cylinder * cylinder,double height) {
	scale(&cylinder->face2Center, 1, cylinder->face2Normals, CYL_SIDE_POINT_NUM, 1.0, height, 1.0);
}


void addFacePoint (G3Xpoint face[], int position, double x , double y, double z) {
	
	G3Xpoint*  vertice = &face[position];
	
	(*vertice)[0] = x;
	(*vertice)[1] = y;
	(*vertice)[2] = z;
}

void gl_Cylinder( double ray, double thinnes)
{

 	int i;
 	int coupe =  20;
	double pas=2*PI/coupe;
	double a=0.,cosa,sina;
  glBegin(GL_QUAD_STRIP);

  for (i=0;i <= coupe; i++)
  {
	cosa=ray* cos(a);
	sina=ray* sin(a);
  	a+=pas;
    glNormal3d(cosa, sina, 0.0);
    glVertex3d(cosa, sina,-thinnes);
    glVertex3d(cosa, sina,+thinnes);
  }

  glEnd();
}

void drawCylinder( Cylinder * cylinder)
{
	int i;

	/* build the side */ 
 
 
  for (i = 0; i < CYL_SIDE_POINT_NUM -1 ; i++)
  {
	  glBegin(GL_QUADS);
	  G3Xpoint*  face1Point = &cylinder->face1Points[i];
	  glNormal3d( cylinder->sideNormals[i][0], cylinder->sideNormals[i][1],  cylinder->sideNormals[i][2] );
	  drawPoint (cylinder->face1Points, i);
	  drawPoint (cylinder->face2Points, i);
	  drawPoint (cylinder->face2Points, i+1);
	  drawPoint (cylinder->face1Points, i+1);
	  
	  glEnd();
  }

	  glBegin(GL_QUADS);
	  G3Xpoint*  face1Point = &cylinder->face1Points[i-1];
	  glNormal3d( cylinder->sideNormals[i][0], cylinder->sideNormals[i][1],  cylinder->sideNormals[i][2]);
	  drawPoint (cylinder->face1Points, i);
	  drawPoint (cylinder->face2Points, i);
	  drawPoint (cylinder->face2Points, 0);
	  drawPoint (cylinder->face1Points, 0);
	  
	  glEnd();
	  
 
  
  /* build the up and lows heads */
  
  
  for (i = 0; i <  CYL_SIDE_POINT_NUM  ; i++)
  {
		glBegin(GL_TRIANGLES);

		/* face 1*/
	
		glNormal3dv(cylinder->face1Normals[i]);
  		glVertex3f( cylinder->face1Center[0], cylinder->face1Center[1], cylinder->face1Center[2]) ;
		drawPoint (cylinder->face1Points, i);
		
		if ( i+1 == CYL_SIDE_POINT_NUM)
			drawPoint (cylinder->face1Points, 0);
		else 	
			drawPoint (cylinder->face1Points, i+1);
		
		glEnd();  
		
		glBegin(GL_TRIANGLES);

		/* face 2*/
 		glVertex3f( cylinder->face2Center[0], cylinder->face2Center[1], cylinder->face2Center[2]) ;
		drawPoint (cylinder->face2Points, i);
		glNormal3dv(cylinder->face2Normals[i]);
		if ( i+1 == CYL_SIDE_POINT_NUM)
			drawPoint (cylinder->face2Points, 0);
		else 	
			drawPoint (cylinder->face2Points, i+1);
		
		glEnd();  
	
  }
  
 

}



void build_cylinder( Cylinder * cylinder)
{

	int i, j;
	double pas=2*PI/CYL_SIDE_SLICE_NUM;
	double a = 0.,cosa,sina;
	memset ( cylinder->face1Center, 0, sizeof ( G3Xpoint)); 
	memset ( cylinder->face2Center, 0, sizeof ( G3Xpoint));
	cylinder->center[0] = 0;
	cylinder->center[1] = 0;
	cylinder->center[2] = 0;
	cylinder->face1Center[2] = 1;  
	cylinder->face2Center[2] = -1;
	
	for (i = 0, j = 0; i < CYL_SIDE_POINT_NUM ; i++, j++)
	{
		cosa= cos(a);
		sina= sin(a);
		a+=pas;

		addFacePoint	(cylinder->face1Points, i , cosa, sina, 1);
		addFacePoint	(cylinder->face2Points, i , cosa, sina, -1);
		cylinder->sideNormals[i][0] =  cosa;
		cylinder->sideNormals[i][1] =  sina;
		cylinder->sideNormals[i][0] =  0;
		
	}
	
	for (i = 0; i <  CYL_SIDE_POINT_NUM  ; i++)
	{

		
		if ( i+1 >= CYL_SIDE_POINT_NUM) {
			normalFromABC (&cylinder->face1Normals[i], cylinder->face1Center, cylinder->face1Points[i], cylinder->face1Points[0], -1 )  ;
			normalFromABC (&cylinder->face1Normals[i], cylinder->face2Center, cylinder->face2Points[i], cylinder->face2Points[0] , -1)  ;
		}
		else{
			normalFromABC (&cylinder->face1Normals[i], cylinder->face1Center, cylinder->face1Points[i], cylinder->face1Points[i+1], -1 ) ;
			normalFromABC (&cylinder->face2Normals[i], cylinder->face2Center, cylinder->face2Points[i], cylinder->face2Points[i+1] , -1)  ;
		}
		
	}

  
}
