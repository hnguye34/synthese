#include "../include/Object.h"
#include "../include/quadrilateral.h"


 void build_moutain(Quadrilateral* moutain) {
	
	/* Settings */
	const double xA = 0;
	const double yA = 0;
	const double zA = 0;
	moutain->AC = 2;
	moutain->AB = 2;
	moutain->CD = 2;
	moutain->BAC = PI/4;
	moutain->ACD = PI - moutain->BAC ;
	
	const double xB = xA + moutain->AB;
	const double yB = yA ;
	const double xC = moutain->AC * cos(moutain->BAC) + xA ;
	const double yC =  moutain->AC * sin(moutain->BAC) + yA ;
	
	const double sigma = moutain->ACD - (PI - moutain->BAC );
	const double yD = yA + yC + moutain->CD * sin(sigma) ;
	const double xD = xA + xC + moutain->CD * cos(sigma);
	 
 	G3Xset(moutain->A , xA , yA , 0 );
 	G3Xset(moutain->B , xB , yB , 0 );
 	G3Xset(moutain->C , xC , yC , 0 );
 	G3Xset(moutain->D , xD , yD , 0 );
}

void drawQuadrilateral(Quadrilateral * moutain) {
	
	glBegin(GL_QUAD_STRIP);
    
	
	 drawAPoint (&moutain->B);	
	 drawAPoint (&moutain->A);	
	 	
	 drawAPoint (&moutain->C);		
	 drawAPoint (&moutain->D);
	 
	 
	 drawAPoint (&moutain->B);	
	glEnd();  
	   	
   
}

