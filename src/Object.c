#include "../include/Object.h"



void drawAPoint (const G3Xpoint * point) {
	
	glVertex3f( (*point)[0], (*point)[1], (*point)[2]) ;
}

void drawPoint (G3Xpoint face[], int position) {
	
	G3Xpoint*  vertice = &face[position];
	drawAPoint (vertice);
}

void copyPoint( G3Xpoint * a, G3Xpoint *b )
{
	(*a)[0] = (*b)[0];
	(*a)[1] = (*b)[1];
	(*a)[2] = (*b)[2];
}
void dumpPoint( G3Xpoint * a , const char * text)
{
	printf ( "Point [%s]:(%f,%f,%f)\n", text, (*a)[0], (*a)[1], (*a)[2]);
}


void normalFromABC( G3Xvector* ABC , G3Xpoint * A, G3Xpoint *B , G3Xpoint * C, int sens )
{
	(*ABC)[0]= abs(  ((*B)[1]-(*A)[1])*((*C)[2]-(*A)[2])-((*B)[2]-(*A)[2])*((*C)[1]-(*A)[1]) ) ;
    (*ABC)[1]= sens * ((*B)[2]-(*A)[2])*((*C)[0]-(*A)[0])-((*B)[0]-(*A)[0])*((*C)[2]-(*A)[2]);
    (*ABC)[2]= sens * ((*B)[0]-(*A)[0])*((*C)[1]-(*A)[1])-((*B)[1]-(*A)[1])*((*C)[0]-(*A)[0]);
}

void setVct( G3Xvector* normal , float x, float y, float z)
{
	(*normal)[0]= x;
    (*normal)[1]= y;
    (*normal)[2]= z;
}


void translate(G3Xpoint points[], int pointsNum,  double x, double y, double z) {
   
	int i = 0 ;
	G3Xhmat matD;
	
	g3x_MakeTranslationXYZ(matD, x, y, z);

	for ( i = 0; i < pointsNum ; i++) 
	{
		
		g3x_ProdHMatPoint(matD, points[i], points[i]);
	/*	printf("-->Après:");
		dumpPoint (points[i]);*/
	}
	
}



void rotate(G3Xpoint points[], int pointsNum,  G3Xvector normals[], int normalsNum,  double angle, AXIS axis) {
    
    G3Xhmat matDirect;
    
    switch ( axis ) {
		case AXIS_X:
			g3x_MakeRotationX(matDirect, angle);
		break;
		case AXIS_Y:
			g3x_MakeRotationY(matDirect, angle);
		break;
		case AXIS_Z:
			g3x_MakeRotationZ(matDirect, angle);
		break;
		default:
			printf ( "ERROR in rotate: the paramter axis not correct.\n") ;
			printf ( "ERROR: recevied parameter  : %d ( point num=%d, normal=%d, angle=%f)\n", (int)axis, pointsNum, normalsNum,  angle) ;
			exit (1);
	}
	
    G3Xvector * v;
    int i = 0;
 /*   printf("-->Avant (%f, %f, %f)\n", points[0][0], points[0][1], points[0][2] );
	*/	
    for ( i = 0; i < pointsNum ; i++) 
	{
		g3x_ProdHMatPoint(matDirect, points[i], points[i]);
	}
	
	for ( i = 0;  i < normalsNum ; i++) 
		g3x_ProdHMatVector(matDirect, normals[i], normals[i]);
  
	/*printf("-->Avant (%f, %f, %f)\n", points[0][0], points[0][1], points[0][2] );
	*/
}


void scale(G3Xpoint points[], int pointsNum, G3Xvector normals[], int normalsNum, double x, double y, double z) {
 

	int i = 0 ;
	G3Xhmat matDirect;
	
	g3x_MakeHomothetieXYZ(matDirect, x, y, z);
		
	for ( i = 0; i < pointsNum ; i++) 
		g3x_ProdHMatPoint(matDirect, points[i], points[i]);
		
	for ( i = 0;  i < normalsNum ; i++) 
		g3x_ProdHMatVector(matDirect, normals[i], normals[i]);	
	
}

void copyColor(G3Xcolor dest, G3Xcolor color){
    int i;
     for(i = 0; i < 4;i++){
             dest[i]=color[i];
     }
}
