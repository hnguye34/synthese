#include <stdio.h>
#include <g3x.h>
#include <tetrahedron.h>


void draw_TertreFace (Face * face ) {
	
	int i = 0;
	
	glBegin(GL_QUADS);
		glNormal3dv( face->normal);
		drawAPoint(face->base1);
		drawAPoint(face->base2);	
		drawAPoint(face->sidePoint1[0]);
		drawAPoint(face->sidePoint2[0]);	
	glEnd();
		
	for ( ; i + 1 < TETRA_SIDE_POINTS_MAXNUM ; i++ ) {
		glBegin(GL_QUADS);
		glNormal3dv( face->normal);
		drawAPoint(face->sidePoint1[i]);
		drawAPoint(face->sidePoint2[i]);	
		drawAPoint(face->sidePoint2[i+1]);
		drawAPoint(face->sidePoint1[i+1]);
		glEnd();

	}
	glBegin(GL_TRIANGLES);
	glNormal3dv( face->normal);
	drawAPoint(face->top);
	drawAPoint(face->sidePoint1[i]);
	drawAPoint(face->sidePoint2[i]);
	glEnd();
	
	
}

void drawTetrahedron(Tetrahedron * tetrahedron)
{
    int i = 0;
    
    for ( ;  i < 4 ; i++ ) {
		draw_TertreFace ( &tetrahedron->faces[i]);
	}
}

/* Equation :
x = x0 + ta
y = y0 + tb
z = z0 + tc.
*/

void buildSidePoint(Face*  face, G3Xpoint points[],  G3Xpoint * base) {

    const double a =  (*base)[0] - face->top[0];
    const double b =  (*base)[1] - face->top[1];
    const double c =  (*base)[2] - face->top[2];
    const double sideLen = sqrt ( pow( a ,2) + pow( b, 2) + pow( c, 2) ) ;
    const double step = (double)1 / (TETRA_SIDE_POINTS_MAXNUM);
    G3Xvector vtDirecteur = {  a, b, c } ;
    	
  
    int i = 0;
    double t = 0;
    
    for ( ; i < TETRA_SIDE_POINTS_MAXNUM ; i++ , t+= step  ) {
       
        double x = (*base)[0] - t * a;
        double y = (*base)[1] - t * b;
        double z = (*base)[2] - t * c;
        G3Xset( points[i] , x , y, z);
        /* printf ("\n[%d / t = %f]: (%f,%f,%f) / (%f,%f,%f) Step=%f." ,i, t, x, y, z, a ,b, c, step);*/
    }
}

void build_tetraFace(Face*  face, const G3Xpoint * top,  const G3Xpoint * base1, const G3Xpoint * base2 ) {
   
    if ( base2 == NULL)
   {
	   printf("==>RROR\n");
	   return;
   }
   
    copyPoint( &(face->top), top);
    copyPoint( &(face->base1), base1);
    
    if ( base2 == NULL)
   {
	   printf("RRROR\n");
	   return;
   }
    copyPoint( &(face->base2), base2);

	
    buildSidePoint(face, face->sidePoint1, &face->base1);
    buildSidePoint(face, face->sidePoint2, &face->base2);
  

}



void build_tetrahedron(Tetrahedron * tetrahedron, double angle) {
   
    const double diagonalDistance = TETRA_DISTANCE_TOP_TO_CENTER * tan(angle);
    const double x = 2 * diagonalDistance * sin( PI / 4);
    const double z = x;
   
    G3Xset(tetrahedron->top, 0, TETRA_DISTANCE_TOP_TO_CENTER, 0);

    G3Xpoint point1 = { x, -TETRA_DISTANCE_TOP_TO_CENTER, z } ;
    G3Xpoint point2 = { x, -TETRA_DISTANCE_TOP_TO_CENTER,-z } ;
   
	build_tetraFace( &tetrahedron->faces[0], &tetrahedron->top, &point1, &point2);
	normalFromABC ( &tetrahedron->faces[0].normal, &tetrahedron->faces[0].top, &tetrahedron->faces[0].sidePoint1, &tetrahedron->faces[0].sidePoint2, 1);

    G3Xpoint point3 = {-x, -TETRA_DISTANCE_TOP_TO_CENTER, z};
    build_tetraFace(&tetrahedron->faces[1], &tetrahedron->top, &point1, &point3);
	normalFromABC ( &tetrahedron->faces[1].normal, &tetrahedron->faces[1].top, &tetrahedron->faces[1].sidePoint1, &tetrahedron->faces[1].sidePoint2, -1);

    G3Xpoint point4 = {-x, -TETRA_DISTANCE_TOP_TO_CENTER, -z};
    build_tetraFace(&tetrahedron->faces[2], &tetrahedron->top, &point3, &point4);
	normalFromABC ( &tetrahedron->faces[2].normal, &tetrahedron->faces[2].top, &tetrahedron->faces[2].sidePoint1, &tetrahedron->faces[2].sidePoint2, -1);

    build_tetraFace(&tetrahedron->faces[3], &tetrahedron->top, &point4, &point2);
	normalFromABC ( &tetrahedron->faces[3].normal, &tetrahedron->faces[3].top, &tetrahedron->faces[3].sidePoint1, &tetrahedron->faces[2].sidePoint2, -1);

}
