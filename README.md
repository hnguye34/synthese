BUT:
Il s'agit de deux applications 3D:
- La première permet d'afficher une scène des divers jolis objets géométriques en 3D (cube, globale, pyramide...) en mouvement
- La deuxième afficher la scène d'un avion de chasse qui tourne autour de la terre qui tourne sur elle-même . ( https://www.youtube.com/watch?v=wu59j0Le2pM&feature=youtu.be)

- Sans maillage:
![sans maillage](./image/obj_cano_plein.jpg)
- Avec de faible maillage:
![avec faible maillage](./image/obj_cano_maillage.jpg)
- Avec un maillage stable:
![avec maillage stable](./image/obj_cano_maillage_stable.jpg)


Ces applications ont été développées avec les libraires OpenGL
